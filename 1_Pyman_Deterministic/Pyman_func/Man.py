import numpy, time
from sys import exit
import matplotlib.pyplot as plt
from math import pi
numpy.set_printoptions(threshold=numpy.nan)

def get_Us(ninc,nbptstroncon,Ups,Amax):
	Us = numpy.zeros((ninc,nbptstroncon))
	coefsUs = Ups[:, ::-1]
	ass=Amax*numpy.arange(0,nbptstroncon)*(1.0/(nbptstroncon-1))
	for i in range(0,ninc): Us[i,:] = numpy.polyval(coefsUs[i,:], ass)
	return Us
def get_Ustab(check,eigen):
	Ustab=numpy.vstack(( (check.T).squeeze(),eigen.T))
	return Ustab

def Branche(U,Ut,nt,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,problem):
	import Preliminar, SysHB,Plotting,Series,Plotting,Stability
	from matplotlib.pyplot import show
	
	ob=open("data","ab")
	
	HH=varstab[-1]
	nbddlstab=len(varstab)-3
	#nbddl=2*M.shape[0]   #size of Jacobian
	if varstab[-2] == varstab[-3]: NeqHH0 = (ninc-1) / (2*(HH)+1)
	else: NeqHH0 = (ninc-2) / (2*(HH)+1)
	
	for i in range(1,nt +1):
		
			if stepcorr==1: U=Preliminar.correction(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuilcorr,itemax,problem)
	
			Ups,FpnlA=Series.calculate_series(U,Ut,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,randvect,chemin,ordre,problem)
			
			if numpy.linalg.norm(FpnlA) == 0: Amax = 1.0
			else: Amax = (seuil/numpy.linalg.norm(FpnlA))**(1.0/(ordre+1))
			
			if SS !=0:  #Stability analysis
				ddlstab =Stability.troncon_ddlstab(ordre,Ups,nbddlstab,varstab,NeqHH0,Amax,nbptstroncon,HH)
				check, eigen=Stability.stabF(nbptstroncon,nbddl,HH,ddlstab,nbddlstab,M,C,K,Gamma,tolstab)
				Ustab=get_Ustab(check,eigen)
			else:Ustab=numpy.ones((1,nbptstroncon))					
			
			U=Series.eval_serie(ordre,ninc,Amax,Ups)
			#if problem=="Vibro-impact": U[Neq-1]=numpy.exp(-Gamma)
			Ut=Series.tangente_serie(ordre,ninc,Amax,Ups)

			R=SysHB.get_R(U,H,Neq,M,K,C,Gamma,ff,Pert,problem)
			print "Tr.", i, "finished  --  ||R||", numpy.linalg.norm(R), "Amax", Amax
			#print U[0],U[2]		
			
			Us=get_Us(ninc,nbptstroncon,Ups,Amax)
			numpy.save(ob,Us)
			
			
			lastfreq, Hand=Plotting.plotHBM(H,Neq,Us,Ustab,ukvar,Hdisp)
			if lastfreq>=finalfreq*2*pi: break
				
	ob.close()
	Leg=[None]*len(Hdisp)      #Set legend and labels
	for j in range(0,len(Hdisp)): Leg[j]="H"+str(Hdisp[j])+  " u"+str(ukvar[j])
	plt.legend(Hand, Leg,loc=2)
	plt.xlabel('Excitation Frequency (Hz)')
	plt.ylabel('Amplitude')
	plt.yscale('log')
	show()
