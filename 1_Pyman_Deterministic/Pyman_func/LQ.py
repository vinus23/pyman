import numpy
from sys import exit
def pL(U,M,K,C,Neq,gamma,problem):
	U=numpy.matrix(U) #type of U-->matrix (bettee for matrix multiply)
	pL=numpy.matrix(numpy.zeros((Neq,1)))

	if problem=="Duffing":
		pL[0:Neq/3,0]=  U[Neq/3:Neq/3*2,0]
		pL[Neq/3:Neq/3*2,0]= numpy.linalg.inv(M)*( -C*U[Neq/3:Neq/3*2,0] -K*U[0:Neq/3,0])
		pL[Neq/3*2:,0]=U[Neq/3*2:Neq,0]
	if problem=="Unilateral_contact":
		dof=(Neq-1)/2
		alpha=gamma[0,0]
		xlim=gamma[1,0]
		pL[0:dof,0]= U[dof:dof*2,0]
		pL[dof:dof*2,0]=numpy.linalg.inv(M)*(-C*U[dof:dof*2,0]-K*U[:dof,0]  -  numpy.vstack(( U[Neq-1,0],[0]   ))  )
		pL[-1,0]=alpha*xlim*U[Neq-1,0]
	if problem=="Bilateral_contact":
		dof=(Neq-2)/2
		alpha=gamma[0,0]
		xlim=gamma[1,0]
		eta=gamma[2,0]
		pL[0:dof,0]= U[dof:dof*2,0]
		pL[dof:dof*2,0]=numpy.linalg.inv(M)*(-C*U[dof:dof*2,0]-K*U[:dof,0]  -  numpy.vstack(( U[Neq-2,0],[0]   ))  )
		pL[-2,0]=alpha**2*(xlim**2*U[Neq-2,0] -eta*U[0,0])
		pL[-1,0]=U[Neq-1,0]
	if problem=="Friction":
		dof=(Neq-2)/2
		p=gamma[0,0]
		mu=gamma[1,0]
		eta=gamma[2,0]
		pL[0:dof,0]= U[dof:dof*2,0]
		pL[dof:dof*2,0]=numpy.linalg.inv(M)*(-C*U[dof:dof*2,0]-K*U[:dof,0]  -  mu*p*numpy.vstack(( U[Neq-2,0],[0.0] )) )
		pL[Neq-2]=U[2,0]
		pL[Neq-1]=0
	if problem=="Vibro-impact":
		dof=(Neq-1)/2
		pL[0:dof,0]= U[dof:dof*2,0]
		pL[dof:dof*2,0]=numpy.linalg.inv(M)*(-C*U[dof:dof*2,0]-K*U[:dof,0] -U[Neq-1,0]      )
	pL=numpy.array(pL)
	return pL
 
def pQ(U,V,gamma,M,C,K,Neq,problem):
	pQ=numpy.matrix(numpy.zeros((Neq,1)))
	U=numpy.matrix(U)
	V=numpy.matrix(V)
	if U.shape[0] !=Neq:
		U=U.T
		V=V.T
	if problem=="Duffing":
		pQ[Neq/3:Neq/3*2,0]= -gamma*numpy.matrix(numpy.multiply(U[0:Neq/3,0],V[Neq/3*2:Neq,0]))	
		pQ[Neq/3*2:,0]= -numpy.matrix(numpy.multiply(U[0:Neq/3,0],V[0:Neq/3,0]))
	if problem=="Unilateral_contact":
		dof=(Neq-1)/2
		gamma=numpy.matrix(gamma)
		xlim=gamma[1,0]
		alpha=gamma[0,0]
		pQ[-1,0]=U[Neq-1,0]*V[Neq-1,0] - U[Neq-1,0]*alpha*V[0,0]
	if problem=="Bilateral_contact":
		dof=(Neq-3)/2
		alpha=gamma[0,0]
		xlim=gamma[1,0]
		eta=gamma[2,0]
		pQ[-2,0]=-U[Neq-2,0]*V[Neq-1]
		pQ[-1,0]=-(U[Neq-2,0]-alpha*U[0,0])*(V[Neq-2,0]-alpha*V[0,0])
	if problem=="Friction":
		dof=(Neq-2)/2
		p=gamma[0,0]
		mu=gamma[1,0]
		eta=gamma[2,0]
		pQ[-2,0]= -V[Neq-2,0]*U[Neq-1,0]
		pQ[-1,0]=U[2,0]*V[2,0]-U[Neq-1,0]*V[Neq-1,0]
	if problem=="Vibro-impact":
		dof=(Neq-1)/2
		pQ[Neq-1]=gamma*U[dof,0]*V[Neq-1,0]
	
	pQ=numpy.array(pQ)
	return pQ
  
def pM(U,Neq,problem):
	pM=numpy.matrix(numpy.zeros((Neq,1)))
	U=numpy.matrix(U)
	if U.shape[0] !=Neq: U=U.T
	
	if problem=="Duffing": pM[0:Neq/3*2,0]=U[0:Neq/3*2,0]
	if problem=="Unilateral_contact":
		dof=(Neq-1)/2
		pM[:dof*2,0]=U[:dof*2,0]
	if problem=="Bilateral_contact" :
		dof=(Neq-2)/2
		pM[:dof*2,0]=U[:dof*2,0]
	if problem=="Friction":
		dof=(Neq-2)/2
		pM[:dof*2,0]=U[:dof*2,0]
	if problem=="Vibro-impact": pM=U
	pM=numpy.array(pM)
	return pM

def L0(H,Neq,FF,Gamma,problem):
	L0=numpy.matrix(numpy.zeros((Neq*(2*H+1),1)))
	Gamma=numpy.matrix(Gamma)
	
	if problem=="Duffing": 
		L0[Neq+Neq/3:Neq+Neq/3*2]=FF
	if problem=="Unilateral_contact":
		dof=(Neq-1)/2
		L0[Neq+dof:Neq+dof*2]=FF
		eta=Gamma[-1,0]
		L0[Neq-1,0]=-Gamma[0,0]*eta
	if problem=="Bilateral_contact" :
		dof=(Neq-2)/2
		L0[Neq+dof:Neq+dof*2]=FF
	if problem=="Friction":
		dof=(Neq-2)/2
		eta=Gamma[2,0]
		L0[Neq+dof:Neq+dof*2]=FF
		L0[Neq-1]=eta
	if problem=="Vibro-impact": 
		dof=(Neq-1)/2
		L0[Neq+dof:Neq+dof*2]=FF
		#L0[Neq-1]=-numpy.exp(1/Gamma)
	return L0

############################################################################################################################################################################################################################################
############################################################################################################################################################################################################################################
############################################################################################################################################################################################################################################
def L(H,Neq,M,K,C,U,Gamma,problem):
	indc=numpy.arange(Neq+1,Neq*(2*H-1)+1   +1,2*Neq)  #index des petits vecteurs  uic 
	inds=numpy.arange(2*Neq+1,Neq*(2*H)+1   +1,2*Neq)    #index des petits vecteurs  uis 
	
	L=numpy.zeros((Neq*(2*H+1),1))
	L[:Neq]=pL(U,M,K,C,Neq,Gamma,problem)
	
	for i in range(1,H +1):
		L[Neq+2*(i-1)*Neq+1 -1:2*Neq+2*(i-1)*Neq]  =pL(U[indc[i -1] -1:indc[i -1]+Neq-1],M,K,C,Neq,Gamma,problem)
		L[2*Neq+2*(i-1)*Neq+1 -1:3*Neq+2*(i-1)*Neq]=pL(U[inds[i -1] -1:inds[i -1]+Neq-1],M,K,C,Neq,Gamma,problem)
	
	return L

def Q(H,Neq,gamma,M,C,K,U,V,problem):
	indc=numpy.arange(Neq+1,Neq*(2*H-1)+1   +1,2*Neq)  #index des petits vecteurs  uic 
	inds=numpy.arange(2*Neq+1,Neq*(2*H)+1   +1,2*Neq)    #index des petits vecteurs  uis 
	
	Q=numpy.zeros((Neq*(2*H+1),1))
	
	SOM=numpy.zeros((Neq,1))
	
	for j in range(1,H +1):   #harmonic 0
		SOM= SOM + pQ(U[indc[j -1] -1:indc[j-1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,M,C,K,Neq,problem) + pQ(U[inds[j -1] -1:inds[j-1]+Neq-1],V[inds[j -1] -1:inds[j-1]+Neq-1],gamma,M,C,K,Neq,problem)
	
	Q[:Neq]= 0.5*SOM + pQ(U[:Neq],V[:Neq],gamma,M,C,K,Neq,problem) 
		
	for ih in range(1,H +1):   #other harmonics
		SOMC=numpy.zeros((Neq,1))
		SOMS=numpy.zeros((Neq,1))
		
		for j in range(1,ih):
			SOMC=SOMC +pQ(U[indc[ih-j -1] -1:indc[ih-j -1]+Neq-1],V[indc[j -1] -1:indc[j -1]+Neq-1],gamma,M,C,K,Neq,problem) -pQ(U[inds[ih-j -1] -1:inds[ih-j-1]+Neq-1],V[inds[j -1] -1:inds[j-1]+Neq-1],gamma,M,C,K,Neq,problem)
			SOMS=SOMS +pQ(U[indc[ih-j -1] -1:indc[ih-j -1]+Neq-1],V[inds[j -1] -1:inds[j -1]+Neq-1],gamma,M,C,K,Neq,problem) +pQ(U[inds[ih-j -1] -1:inds[ih-j-1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,M,C,K,Neq,problem)
   	
		for j in range(ih+1,H +1):
			SOMC=SOMC+ pQ(U[indc[j -1] -1:indc[j-1]+Neq-1],V[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],gamma,M,C,K,Neq,problem)+pQ(U[inds[j-1] -1:inds[j-1]+Neq-1],V[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],gamma,M,C,K,Neq,problem) +pQ(U[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,M,C,K,Neq,problem)  +pQ(U[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],V[inds[j-1] -1:inds[j-1]+Neq-1],gamma,M,C,K,Neq,problem)
			SOMS=SOMS+ pQ(U[inds[j -1] -1:inds[j-1]+Neq-1],V[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],gamma,M,C,K,Neq,problem)-  pQ(U[indc[j-1] -1:indc[j-1]+Neq-1],V[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],gamma,M,C,K,Neq,problem) -pQ(U[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,M,C,K,Neq,problem)  +pQ(U[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],V[inds[j-1] -1:inds[j-1]+Neq-1],gamma,M,C,K,Neq,problem)
	
		Q[indc[ih -1] -1:indc[ih -1]+Neq-1]=0.5*SOMC + pQ(U[indc[ih -1] -1:indc[ih -1]+Neq-1],V[:Neq],gamma,M,C,K,Neq,problem)  +pQ(U[:Neq],V[indc[ih -1] -1:indc[ih -1]+Neq-1],gamma,M,C,K,Neq,problem) - ih*V[Neq*(2*H+1)+1 -1]*pM(U[inds[ih -1] -1:inds[ih -1]+Neq-1],Neq,problem)
		Q[inds[ih -1] -1:inds[ih -1]+Neq-1]=0.5*SOMS + pQ(U[inds[ih -1] -1:inds[ih -1]+Neq-1],V[:Neq],gamma,M,C,K,Neq,problem)  +pQ(U[:Neq],V[inds[ih -1] -1:inds[ih -1]+Neq-1],gamma,M,C,K,Neq,problem) + ih*V[Neq*(2*H+1)+1 -1]*pM(U[indc[ih -1] -1:indc[ih -1]+Neq-1],Neq,problem)
	return Q
