import numpy, scipy
from sys import exit
from time import clock

def correction(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuilcorr,itemax,problem):
	import SysHB
	from scipy import linalg
	nbites = 0
	
	R=SysHB.get_R(U,H,Neq,M,K,C,Gamma,ff,Pert,problem)
	if problem=="Vibro-impact": U[Neq-1]=numpy.exp(-Gamma)
	normR = numpy.linalg.norm(R)
	while normR>seuilcorr and nbites<itemax:
				if nbites==0:
					print "Correction started"
				nbites = nbites + 1
				if problem=="Vibro-impact": U[Neq-1]=numpy.exp(-Gamma)
				#start=clock()
				dRdU=SysHB.get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,problem)
				#finish = clock()
				#print 'time is ', finish - start,'s'
							
				Ut=SysHB.get_Ut(U,dRdU,ninc,randvect,chemin)
				KK = numpy.vstack((dRdU,Ut.T))
				R1=numpy.vstack((R,0)) 
				U = U - scipy.linalg.solve(KK,R1)   #UUU=numpy.linalg.solve(K,R1)	
				R=SysHB.get_R(U,H,Neq,M,K,C,Gamma,ff,Pert,problem)
				normR = numpy.linalg.norm(R)
				print "iter n.", nbites, "||R||", normR
				if problem=="Vibro-impact": U[Neq-1]=numpy.exp(-Gamma)
	return U
