import numpy, LQ
numpy.set_printoptions(threshold=numpy.nan)
def get_R(U,H,Neq,M,K,C,Gamma,ff,Pert,problem):
		L0=LQ.L0(H,Neq,ff,Gamma,problem)
		L=LQ.L(H,Neq,M,K,C,U,Gamma,problem)
		Q=LQ.Q(H,Neq,Gamma,M,C,K,U,U,problem)
		R=L0+L+Q+Pert
		return R
		
def get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,problem):

		dRdU = numpy.zeros((neqsys,ninc))
		for c in range(1,ninc +1):
				Ue = numpy.zeros((ninc,1))
				Ue[c-1] = 1
				dRdU[:,c-1] = numpy.transpose(LQ.L(H,Neq,M,K,C,Ue,Gamma,problem) + LQ.Q(H,Neq,Gamma,M,C,K,Ue,U,problem) +LQ.Q(H,Neq,Gamma,M,C,K,U,Ue,problem))
		if problem=="Vibro-impact":dRdU[Neq-1,Neq-1]=1.0
		#print numpy.linalg.cond(dRdU)
		return dRdU
		
def  get_Ut(U,dRdU,ninc,randvect,chemin):
		import scipy
		from math import sqrt
		b =(numpy.zeros((ninc,1)))
		b[-1]=1

		Tmat = numpy.vstack((dRdU,randvect))
		Ut=-scipy.linalg.solve(Tmat,b)   
		chemin=numpy.matrix(chemin)
		uu=numpy.multiply(chemin,Ut)
		#print sqrt(numpy.dot (uu.T,Ut ))
		Ut = Ut *  1/sqrt(numpy.dot (uu.T,Ut ))	#normalisation
		
		return Ut 


