import numpy
from sys import exit
import matplotlib.pyplot as plt
from math import pi

def plotHBM(H,Neq,U,Ustab,Idisp,Hdisp):
	Nb=len(Idisp)
	style = [':','-']
	colour=['b','r','g','y','k','c','m']
	omega = U[-1,:] #frequencies
	lastfreq=max(omega)
	print "Last Frequency", lastfreq/2.0/pi
	stab = Ustab[0,:]
	stab=stab.real
	Ibif=numpy.array((( stab[1:]-stab[:-1] )!=0).nonzero())
	Ibif=Ibif[0,:]
	Ibif=numpy.hstack(( numpy.array((0)),Ibif,numpy.array((U.shape[1] -1))    ))
	Hand=[None]*Nb
	plt.figure(1)
	
	for ib in range(0, Nb):
		idisp=Idisp[ib]
		hdisp=Hdisp[ib]
		col=colour[ib]  #colour for this plot
		
		if hdisp==0: Amp=U[idisp-1,:]
		else:
			Icos=Neq+idisp+(hdisp-1)*2*Neq
			Amp=( (U[Icos -1,:])**2+(U[Icos+Neq -1,:])**2)**(0.5) #  sqrt(Hcos**2+Hsin**2)
		for bb in range(1,len(Ibif)-1 +1):
			II=range(Ibif[bb -1],Ibif[bb] +1)
			st=style[int(stab[Ibif[bb-1]+1])]
			hand, =plt.plot(0.5/pi*omega[II],Amp[II],linewidth=2)
			plt.setp(hand,'color',col,'ls',st)
		hand1, =plt.plot(0.5/pi*omega[Ibif[1:-1] ],Amp[Ibif[1:-1]],'.')
		plt.setp(hand1,'color',col)
		Hand[ib]=hand
	
	return lastfreq,Hand
