import os, numpy as np, sys,matplotlib.pyplot as plt

ff=open("data","rb")
H=1
Neq=2*2 +2

Hdisp=[1]  # Harmonic you want to display
Udisp=[1]  # Unkown you want to display (dof)


######### PLOT
Us=np.load(ff)
while(1):
	Us=np.hstack((Us,np.load(ff)))
	if ff.tell() == os.fstat(ff.fileno()).st_size: break


for i in range (0,len(Hdisp)):
	hdisp=Hdisp[i]
	udisp=Udisp[i]
	Icos=Neq+udisp+(hdisp-1)*2*Neq-1
	plt.plot(Us[-1]/np.pi/2.0,np.sqrt(Us[Icos]**2+Us[Icos + Neq]**2))

plt.yscale('log')
plt.show()

ff.close()

