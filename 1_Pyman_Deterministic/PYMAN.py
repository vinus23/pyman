###ANM
import os, sys
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Pyman_func" )
import random,numpy,SysHB,Preliminar,Man
###############################################################################################
H=1 #nb of harmonics
problem="Duffing"
Hdisp=[1,1]#,1,1,1,1]   #harmonics we want to display
ukvar=[1,2]#,2,3,4,5]  #uknown we want to dispay, (same size for Hdisp and ukvar!!!)
finalfreq=60 #in Hz, where we want to arrive
## Physical Parameters, matrices M,C,K,Gamma(nonlinear coeff.) and vector of Forces
m=1
k=15000.0
c=1.0
K=numpy.array([[2*k,-k],[-k,k]])
C=numpy.array([[2*c,-c],[-c,c]])
M=numpy.array([[m,0],[0,m]])
dof=M.shape[0]
ff=numpy.zeros((dof,1))   
ff[0,0]=1		#Put a force on one or more dof nb
if dof>1: ff=numpy.dot(numpy.linalg.inv(M),ff)
else: ff=ff/m

ntronc=6000	#Nb of steps MAX

# Gamma is a matrix containing the terms used to compute the non linearity for each DOF
if problem=="Duffing":
	Neq=3*dof   ### nb of equations of the system in time domain after reguralisation
	knl=5.0e10
	Gamma=numpy.eye(dof)*0
	Gamma[0,0]=knl
if problem=="Unilateral_contact":
	Neq=2*dof+1
	xlim=1.0e-3
	kc=1.0e4
	eta=1e-9
	Gamma=numpy.zeros((3,1))
	Gamma[0]=kc
	Gamma[1]=xlim
	Gamma[2]=eta
if problem=="Bilateral_contact":
	Neq=2*dof+2
	kc=1.0e4
	xlim=1.0e-3
	eta=1e-10
	Gamma=numpy.zeros((3,1))
	Gamma[0]=kc
	Gamma[1]=xlim
	Gamma[2]=eta
if problem=="Friction": #Coulomb's law
	Neq=2*dof+2
	p=10.0
	mu=0.1
	eta=1.0e-6
	Gamma=numpy.zeros((3,1))
	Gamma[0]=p
	Gamma[1]=mu
	Gamma[2]=eta
if problem=="Vibro-impact":
	Neq=2*dof+1
	alpha=k*10
	Gamma=alpha

#################################################################################################
#################################################################################################
#################################################################################################
ninc=Neq*(2*H+1)+1 #variables (all the harmonics + the frequency)
neqsys=ninc-1
### 
randvect=numpy.random.rand(1,ninc) # it will be used for tangent calculation...see in SYSHB.py
###ANM parameters
chemin = numpy.ones((ninc,1)) # default path vector (weighting function)
ordre = 15;            					#default ANM series order
nbptstroncon = 10    					#default sampling of portions for display
seuil = 1.0e-7        					#default ANM threshold
itemax = 100          					#default max. iteration for N-R correction
seuilcorr = 1.0e-11      				#default threshold for N-R correction
stepcorr=0    							#put 1 to correct at each step
##################################################
###########Perturbation
cfp = 1.0 *0           #switch to 1 (or -1) if you want to perturb
Pertampl=1.0e-9
Fp = numpy.random.rand(neqsys,1) # perturbation vector
Fp = Fp/numpy.linalg.norm(Fp) # normalization
Pert=cfp*Pertampl*Fp
############Stability
SS=0    			#put to 1 if you want the stability analysis
varstab = numpy.hstack(( numpy.arange(1,dof+1) , numpy.array([Neq*(2*H+1)+1, Neq*(2*H+1)+1, H]) )) #Variables used for stability analysis: Dispacements, frequency 
tolstab = 1e-4    #tol used in stability analysis
###
Ustart  = numpy.zeros((Neq*(2*H+1)+1,1))
#Ustart= numpy.random.rand(ninc,1)*eta
#Ustart[Neq-1]=numpy.exp(-Gamma)
if problem=="Friction":
	Ustart[4]=0.0001
	Ustart[5]=0.0001
Ustart[-1]= 0.001 #Starting Frequency!!! in rad/s
print Ustart
###STARTING###
U=Ustart
U=Preliminar.correction(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuilcorr,itemax,problem)
Ut=SysHB.get_Ut(U,SysHB.get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,problem),ninc,randvect,chemin)
if Ut[-1]<0:Ut=-Ut

Man.Branche(U,Ut,ntronc ,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,problem)
