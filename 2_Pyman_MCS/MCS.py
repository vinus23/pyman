###MCS
import matplotlib,PYMAN,numpy,time
import matplotlib.pyplot as plt
from openturns import *
from sys import exit
start= time.clock()
#RandomGenerator.SetSeed(0)

m=1.0
knl=5.0e10
c=1.0
k=15000.0
force=1.0

DIM=3
Nsample=1000
mean=[m,knl,k]
delta=[0.075,0.1,0.025]

Dist=[Uniform(-1,1),Uniform(-1,1),Normal(0,1)]
Sample=[None]*len(Dist)

for i in range(0,len(Dist)):
	RandomExp= MonteCarloExperiment(Dist[i],Nsample)
	Sample[i] =RandomExp .generate( )

for n in range(0,Nsample):
	randvars=numpy.zeros(DIM)
	for i in range(0,DIM):
		randvars[i]=(numpy.array(Sample[i][n])*delta[i]+1)*mean[i]
	
	m=randvars[0]
	knl=randvars[1]
	k=randvars[2]
	
	K=numpy.array([[2*k,-k],[-k,k]])
	C=numpy.array([[2*c,-c],[-c,c]])
	M=numpy.array([[m,0],[0,m]])
	dof=M.shape[0]
	
	Gamma=numpy.eye(dof)*0
	Gamma[0,0]=knl
	
	ff=numpy.zeros((dof,1))   
	ff[0,0]=force	#Put a force on one or more dof
	
	print "\n MCS -- ITERATION NB.", n+1
	PYMAN.start(M,K,C,Gamma,ff,n)
elapsed = (time.clock() - start)
print "Time elapsed MCS", elapsed, "seconds"
