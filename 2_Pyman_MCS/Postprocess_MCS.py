import numpy as np, os, matplotlib.pyplot as plt, glob
from sys import exit
from math import pi


############################################################################################################################################
dof=2
Neq=dof*3
hdisp=1
udisp=[1]
lastfreq=55
folder=os.path.dirname(os.path.realpath(__file__))+"/Data_B"
percen=95
#############################################################################################################################################
###1 Reading the files
print 'Reading files'
Icos=np.zeros(len(udisp))
for j in range(0,len(udisp)): Icos[j]=Neq+udisp[j]+(hdisp-1)*2*Neq
vec=np.arange(5,1000+5,5)

Nsample=len(glob.glob(folder+"/*"))/10

#for Nsample in vec:
Ntr=800   # nb.maxi of tranches to read
#obFile = open(os.path.dirname(os.path.realpath(__file__))+"/MCS"+str(Nsample),"ab")
print "n. samples:", Nsample
AA=[np.zeros(0)]*Nsample
Freq=[np.zeros(0)]*Nsample
Amp=[np.zeros(0)]*Nsample*len(udisp)
finalA=np.zeros(Nsample)

for N in range(0,Nsample):
	fp = open(folder+"/values"+str(N+1),"rb")
	print (N+1)*1.0*100/Nsample,'%'
	Aold=0
	for i in range(0,Ntr):
		Us=np.load(fp)
		Amax=np.linalg.norm(Us[:,-1]-Us[:,0])
		nbptstroncon=Us.shape[1]
		aa=Amax*np.arange(0,nbptstroncon)*(1.0/(nbptstroncon-1))+ Aold
		Aold=aa[-1]
		AA[N]=np.hstack((AA[N],aa))
		Freq[N]=np.hstack((Freq[N],Us[-1,:]/2.0/pi))
		for j in range(0,len(udisp)): Amp[Nsample*j+N]=np.hstack((Amp[Nsample*j+N],((Us[Icos[j] -1,:])**2+(Us[Icos[j]+Neq -1,:])**2)**(0.5)))
		if fp.tell() == os.fstat(fp.fileno()).st_size: break
	fp.close()
	finalA[N]=AA[N][-1]

finalA=min(finalA)
aa=np.arange(0,finalA,finalA/(len(AA[0])*1))
FReq=np.zeros((Nsample,len(aa)))
AMP=np.zeros((Nsample,len(aa),len(udisp)))

for N in range(0,Nsample):
	FReq[N,:]=np.interp(aa,AA[N],Freq[N])
	for j in range(0,len(udisp)): 
		AMP[N,:,j]=np.interp(aa,AA[N],Amp[Nsample*j+N])
		plt.figure(3)
		p1,=plt.plot(FReq[N,:],AMP[N,:,j],'b')

peaks=[None]*Nsample*len(udisp)
retpn=[None]*Nsample*len(udisp)
maxmin=[None]*Nsample*len(udisp)
size=np.zeros(Nsample*len(udisp))
minsize=np.zeros(len(udisp))


for j in range(0,len(udisp)):
	for N in range(0,Nsample):
		retpn[Nsample*j+N]=np.array(np.nonzero(np.diff((np.diff(FReq[N,:])>0)*1)!=0))[0,:]+1
		peaks[Nsample*j+N]=np.array(np.nonzero(np.diff((np.diff(AMP[N,:,j])>0)*1)!=0))[0,:]+1
		#print peaks[Nsample*j+N],retpn[Nsample*j+N]
tol=30

for j in range(0,len(udisp)):
	for N in range(0,Nsample):
		while(1):
			diff=np.diff(peaks[Nsample*j+N])
			if min(diff)<tol:
				indmin=np.argmin(diff)
				valmin=peaks[Nsample*j+N][indmin]
				peaks[Nsample*j+N]=np.array(np.delete(peaks[Nsample*j+N],indmin))
				indmin=np.argmin(abs(valmin-peaks[Nsample*j+N]))
				peaks[Nsample*j+N]=np.array(np.delete(peaks[Nsample*j+N],indmin))
			else: break

for j in range(0,len(udisp)):
	for N in range(0,Nsample):
		while(1):
			diff=np.diff(retpn[Nsample*j+N])
			if min(diff)<tol:
				indmin=np.argmin(diff)
				valmin=retpn[Nsample*j+N][indmin]
				retpn[Nsample*j+N]=np.array(np.delete(retpn[Nsample*j+N],indmin))
				indmin=np.argmin(abs(valmin-retpn[Nsample*j+N]))
				retpn[Nsample*j+N]=np.array(np.delete(retpn[Nsample*j+N],indmin))
			else: break

for j in range(0,len(udisp)):
	for N in range(0,Nsample):
		maxmin[Nsample*j+N]=np.sort(np.hstack((peaks[Nsample*j+N],retpn[Nsample*j+N])))
		print maxmin[Nsample*j+N]

colstd='Purple'
colper='GreenYellow'
### 2 Mean and std deviation
print 'Mean and std deviation'
sig=1.0
vv=7
if Nsample==5:vv=0
for j in range(0,len(udisp)):
	ind=np.zeros(Nsample)
	for r in range(0,len(maxmin[vv])	):
		print 1.0*(r+1)/len(maxmin[vv])*(j+1)/len(udisp)*100, '%'
		nstep=int((int(maxmin[Nsample*j+vv][r])-ind[vv]+1)*2)
		freq=np.zeros((Nsample,nstep))
		amp=np.zeros((Nsample,nstep))
		supp=np.zeros((Nsample,nstep))
		for N in range(0,Nsample):
			supp[N,:]=np.linspace(aa[ind[N]],aa[maxmin[Nsample*j+N][r]],nstep)
			freq[N,:]=np.interp(supp[N,:],aa[ind[N]:maxmin[Nsample*j+N][r]+1],FReq[N,ind[N]:maxmin[Nsample*j+N][r]+1])
			amp[N,:]=np.interp(supp[N,:],aa[ind[N]:maxmin[Nsample*j+N][r]+1],AMP[N,ind[N]:maxmin[Nsample*j+N][r]+1,j])
			ind[N]=maxmin[Nsample*j+N][r]
		meansupp=sum(supp)/Nsample
		meanamp=sum(amp)/Nsample
		meanfreq=sum(freq)/Nsample
		stdamp=np.std(amp,axis=0)
		stdfreq=np.std(freq,axis=0)
		stdsupp=np.std(supp,axis=0)
		perampup=np.percentile(amp,percen,axis=0)
		perampbot=np.percentile(amp,100-percen,axis=0)
		perfrequp=np.percentile(freq,percen,axis=0)
		perfreqbot=np.percentile(freq,100-percen,axis=0)
		
		#np.save(obFile,meanfreq)
		#np.save(obFile,meanamp)
		
		for i in range(0,nstep):
			if min(( (np.percentile(freq[:,i],100)-freq[:,i])**2 + (np.percentile(amp[:,i],100)-amp[:,i])**2)**0.5)>min(( (np.percentile(freq[:,i],100)-freq[:,i])**2 + (np.percentile(amp[:,i],0)-amp[:,i])**2)**0.5):
				stdamp[i]=-stdamp[i]
				temp=perampbot[i]
				perampbot[i]=perampup[i]
				perampup[i]=temp
		plt.figure(3)
		plt.plot(meanfreq,meanamp,'y',linewidth=2)
		plt.plot(meanfreq+stdfreq,meanamp+stdamp*sig,colstd,linewidth=1.5)
		plt.plot(meanfreq- stdfreq,meanamp-stdamp*sig,colstd,linewidth=1.5)
		plt.plot(perfrequp,perampup,colper,linewidth=1.5,linestyle='--')
		plt.plot(perfreqbot,perampbot,colper,linewidth=1.5,linestyle='--')
		#plt.figure(5)
		#plt.plot(meansupp,abs(stdamp))
	finalA=len(aa)
	nstep=int((finalA-ind[0]+1)*1.2)
	freq=np.zeros((Nsample,nstep))
	amp=np.zeros((Nsample,nstep))
	supp=np.zeros((Nsample,nstep))
	for N in range(0,Nsample):
		supp[N,:]=np.linspace(aa[ind[N]],aa[-1],nstep)
		freq[N,:]=np.interp(supp[N,:],aa[ind[N]:],FReq[N,ind[N]:])
		amp[N,:]=np.interp(supp[N,:],aa[ind[N]:],AMP[N,ind[N]:,j])
		ind[N]=maxmin[N][r]
	meansupp=sum(supp)/Nsample
	meanamp=sum(amp)/Nsample
	meanfreq=sum(freq)/Nsample
	stdamp=np.std(amp,axis=0)
	stdfreq=np.std(freq,axis=0)
	stdsupp=np.std(supp,axis=0)
	perampup=np.percentile(amp,percen,axis=0)
	perampbot=np.percentile(amp,100-percen,axis=0)
	perfrequp=np.percentile(freq,percen,axis=0)
	perfreqbot=np.percentile(freq,100-percen,axis=0)
	#np.save(obFile,meanfreq)
	#np.save(obFile,meanamp)
	for i in range(0,nstep):
		if min(( (np.percentile(freq[:,i],100)-freq[:,i])**2 + (np.percentile(amp[:,i],100)-amp[:,i])**2)**0.5)>min(( (np.percentile(freq[:,i],100)-freq[:,i])**2 + (np.percentile(amp[:,i],0)-amp[:,i])**2)**0.5):
			stdamp[i]=-stdamp[i]
			temp=perampbot[i]
			perampbot[i]=perampup[i]
			perampup[i]=temp
	plt.figure(3)
	p2,=plt.plot(meanfreq,meanamp,'y',linewidth=2)
	p3,=plt.plot(meanfreq+stdfreq,meanamp+stdamp*sig,colstd,linewidth=1.5)
	plt.plot(meanfreq- stdfreq,meanamp-stdamp*sig,colstd,linewidth=1.5)
	p4,=plt.plot(perfrequp,perampup,colper,linewidth=1.5,linestyle='--')
	plt.plot(perfreqbot,perampbot,colper,linewidth=1.5,linestyle='--')
	#plt.figure(5)
	#plt.plot(meansupp,abs(stdamp))
	#obFile.close()

plt.figure(3)
plt.legend([p1,p2,p3,p4],["Stochastic response","Mean","Mean + - std. dev.","95% and 5% percentiles"],loc=4)
plt.xlim((0,lastfreq))
plt.ylim((1e-6,1e-2))
plt.yscale('log')
plt.title("DOF nb."+str(udisp[0]))
plt.xlabel("Excitation frequency [Hz]")
plt.ylabel("Amplitude [m]")
plt.yscale('log')
plt.show()
