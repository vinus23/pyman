import numpy
#from sys import exit
#numpy.set_printoptions(threshold=numpy.nan)
def calculate_series(U,Ut,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,randvect,chemin,ordre):
		import LQ, scipy, SysHB
		pmax=ordre
		dRdU=SysHB.get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff)
		U1=SysHB.get_Ut(U,dRdU,ninc,randvect,chemin)
		
		if numpy.dot(U1.T,Ut)<0:
			U1=-U1
		
		KK = numpy.vstack((dRdU,numpy.transpose(numpy.multiply(chemin,Ut)) ))	    # Matrice tangente de la MAN
		Ups = numpy.zeros((ninc,pmax+1))
		Ups[:,0] = numpy.transpose(U) #Point de depart
		Ups[:,1] = numpy.transpose(U1) #Premier Ordre 
		lu=scipy.linalg.lu_factor(KK)
		
		for p in range(2,pmax +1):
				Fpnl = numpy.zeros((ninc,1))
				for r in range(1,p-1 +1):
						Fpnl = Fpnl - numpy.vstack((  LQ.Q(H,Neq,Gamma,Ups[:,r],Ups[:,p-r]),  [0]))            #attention a Ups (transpose...)
				Ups[:,p]=(scipy.linalg.lu_solve(lu,Fpnl)).T
				#Ups[:,p] = (scipy.linalg.solve(K, Fpnl)).T    # Pour p=2 on a Ups = [U0, U1, U2]
		
		FpnlA = numpy.zeros((neqsys,1))
		for r in range (1,p +1):
				FpnlA=FpnlA - LQ.Q(H,Neq,Gamma,Ups[:,r],Ups[:,p-r+1])
			
		return Ups,FpnlA

def eval_serie(ordre,ninc,Amax,Ups):   #Evaluation de la series Ups a l'ordre
  coefs = numpy.zeros((ordre+1,1))
  U = numpy.zeros((ninc,1))
  ass = numpy.zeros((ordre+1, 1)) #Creation de la liste des coef dans le bon ordre
  t = 1
  for p in range(1,ordre+1 +1):
	  ass[p -1,0] = t
	  t = t * Amax
  U = numpy.dot(Ups,ass)
  return U

def tangente_serie(ordre,ninc,Amax,Ups):
	Ut = numpy.zeros((ninc, 1))
	for i in range(1,ordre-1 +1):
		Ut = Ut + i * Amax**(i-1) * numpy.reshape(Ups[:,i],(ninc,1),'F')
	return Ut

def eval_tronc(nbcourbes,Amax,ordre,Ups,nbptstroncon,dispvars):
	X = numpy.zeros((nbptstroncon, nbcourbes)) #ascisse
	Y = numpy.zeros((nbptstroncon, nbcourbes)) # Ordinate axis parameter
	A = numpy.zeros((nbptstroncon, nbcourbes)) # A values for each (X,Y) point
	coefsX = numpy.zeros((ordre+1,1))
	coefsY = numpy.zeros((ordre+1,1))
	for i in range(0,nbcourbes):
		idxX = dispvars[i,0]
		idxY = dispvars[i,1]
		for p in range(0,ordre +1):
			coefsX[p,0] = Ups[idxX -1, ordre-p]
			coefsY[p,0] = Ups[idxY -1, ordre-p]
		for p in range(0,nbptstroncon):
			a = p *Amax/ (nbptstroncon-1)
			A[p,i]=a
			X[p,i] = numpy.polyval(coefsX, a)
			Y[p,i] = numpy.polyval(coefsY, a)
	return X,Y,A
