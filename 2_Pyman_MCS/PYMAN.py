###ANM
def start(M,K,C,Gamma,ff,ii):
	import os, sys
	sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Pyman_func" )
	import random,numpy,SysHB,Preliminar,Man
	invM=numpy.linalg.inv(M)
	###
	H=3       #nb of harmonics
	###Physical Parameters, matrices M,C,K,Gamma(nonlinear coeff.) and vector of Forces
	dof=M.shape[0]
	ff=numpy.dot(invM,ff)
	###
	finalfreq=60  #in Hz, where we want to arrive
	Hdisp=[1]   #harmonics we want to display
	ukvar=[2]  #uknown we want to dispay, (same size for Hdisp and ukvar!!!)
	###
	Neq=3*dof #n of equations
	ninc=Neq*(2*H+1)+1 #variables (all the harmonics + the frequency)
	neqsys=ninc-1
	### 
	randvect=numpy.random.rand(1,ninc) # it will be used for tangent calculation...see in SYSHB.py
	###ANM parameters
	chemin = numpy.ones((ninc,1)) # default path vector (weighting function)
	ordre = 15;            					#default ANM series order
	nbptstroncon = 20    					#default sampling of portions for display
	seuil = 1.0e-7           					#default ANM threshold
	itemax = 15            					#default max. iteration for N-R correction
	seuilcorr = 1.0e-8      				#default threshold for N-R correction
	stepcorr=0    							#put 1 to correct at each step
	ntronc=300		   						#Nb of steps MAX
	###
	###Perturbation
	cfp = 0.0            #switch to 1 (or -1) if you want to perturb
	Pertampl=1.0e-6
	Fp = numpy.random.rand(neqsys,1) # perturbation vector
	Fp = Fp/numpy.linalg.norm(Fp) # normalization
	Pert=cfp*Pertampl*Fp
	###Stability
	SS=0    			#put to 1 if you want the stability analysis
	varstab = numpy.hstack(( numpy.arange(1,dof+1) , numpy.array([Neq*(2*H+1)+1, Neq*(2*H+1)+1, H]) )) #Variables used for stability analysis: Dispacements, frequency 
	tolstab = 1e-4    #tol used in stability analysis
	###
	Ustart  = numpy.zeros((Neq*(2*H+1)+1,1))
	Ustart[-1]= 0.01 #Starting Frequency!!! in rad/s
	###STARTING###
	U=Ustart
	U=Preliminar.correction(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuilcorr,itemax)
	Ut=SysHB.get_Ut(U,SysHB.get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff),ninc,randvect,chemin)
	obFile = open(os.path.dirname(os.path.realpath(__file__))+"/Data/"+"values"+str(ii+1),"ab")
	Man.Branche(U,-Ut,ntronc ,H,Neq,ninc,neqsys,invM,K,C,Gamma,ff,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,obFile)
	
