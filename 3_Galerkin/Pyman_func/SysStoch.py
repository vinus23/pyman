import sympy, numpy as np,GQuad,LQ,matplotlib.pylab as plt
from sys import exit

def L0(P,nodes,weights,H,Neq,ff):
	L0HB=LQ.L0(H,Neq,ff)
	PSI=GQuad.get_PSI(nodes,P,"hermite")
	PSI=PSI.T
	normfact=GQuad.get_normfact(P)

	L0W=np.matrix(np.zeros((L0HB.shape[0],len(weights))))
	for i in range(0,len(weights)):
		L0W[:,i] = np.array(np.array(L0HB.subs('xi',nodes[i])), np.float) *weights[i]
		
	L0=np.dot(L0W,PSI)
	L0=np.reshape(L0,(L0HB.shape[0]*(P+1),1),'F')
	L0=np.vstack((L0,np.zeros((P,1))))
	
	for i in range(L0.shape[0]):
		if abs(L0[i])<1e-7: L0[i]=0
		
	return L0
	
def L(P,nodes,weights,Neq,H,M,K,C):
	A=sympy.Matrix(np.zeros((Neq*(2*H+1) , Neq*(2*H+1) +1 )) )   #linear application for HB
	Nc=len(weights)
	
	for i in range(0,A.shape[1]):
		U=sympy.Matrix(np.zeros((Neq*(2*H+1) +1, 1)) )
		U[i,0]=1
		A[:,i]=LQ.L(H,Neq,M,K,C,U)
		
	PSI=GQuad.get_PSI(nodes,P,"hermite")
	
	L=np.zeros((Neq*(2*H+1)*(P+1),A.shape[1]*(P+1) ))
	Anw=np.zeros((A.shape[0],A.shape[1]*Nc ))
	for i in range(0,Nc):
		Anw[:, A.shape[1]*i:A.shape[1]*(i+1)]=np.array(np.array(A.subs('xi',nodes[i])), np.float)*weights[i]

	for i in range(0,P+1) : 
		for j in range(i,P+1): 
			for n in range (0,Nc):
				L[Neq*(2*H+1)*i:Neq*(2*H+1)*(i+1),A.shape[1]*j:A.shape[1]*(j+1)]  =  L[Neq*(2*H+1)*i:Neq*(2*H+1)*(i+1),A.shape[1]*j:A.shape[1]*(j+1)]  + Anw[:, A.shape[1]*n:A.shape[1]*(n+1)]*PSI[i,n]*PSI[j,n]
			if j!=i: L[Neq*(2*H+1)*j:Neq*(2*H+1)*(j+1),A.shape[1]*i:A.shape[1]*(i+1)] = L[Neq*(2*H+1)*i:Neq*(2*H+1)*(i+1),A.shape[1]*j:A.shape[1]*(j+1)]
	
	L=np.vstack(( L,np.zeros((P,L.shape[1])))) # stochastic phase condition (add zeros at the bottom of the linear part)
	print "Computation of stochastic linear operator finished"
	L[abs(L)<1e-7]=0  #tollerance!!!!!!!!!!!!!!!!!!
	
	#aa=np.array(np.nonzero(L!=0)) # to plot the matrix
	#plt.plot(aa[0],-aa[1],'*')
	#plt.show()
	#exit()
	return L

def Q(P,nodes,weights,H,Neq,Gamma):
	import scipy.sparse
	
	A=sympy.Matrix(np.zeros((Neq*(2*H+1) +1, (Neq*(2*H+1) +1)*Neq*(2*H+1)  ))   )   ### A is a matrix containing the bilinear application matrices such that q(U,U) [i] = U.T*A[i]*U,
	Nc=len(nodes)
	PSI=GQuad.get_PSI(nodes,P,"hermite")
	
	for i in range(0,Neq*(2*H+1) +1):
		U=sympy.Matrix(np.zeros((Neq*(2*H+1) +1, 1)) )
		U[i,0]=1
		for j in range(0,Neq*(2*H+1) +1):
			V=sympy.Matrix(np.zeros((Neq*(2*H+1) +1, 1)) )
			V[j,0]=1
			q=LQ.Q(H,Neq,Gamma,U,V)
			for k in range (0,q.shape[0]): A[i, j + k*(Neq*(2*H+1)+1)]=q[k,0]

	Q=scipy.sparse.lil_matrix((   (Neq*(2*H+1) +1)*(P+1), (Neq*(2*H+1) +1)*Neq*(2*H+1)*(P+1)**2  ))
	print "Computation of stochastic quadratic operator" ##before this everthing is ok
	for n in range(0,Neq*(2*H+1)):
		print float(int(100.0*(n+1)/(Neq*(2*H+1))*1000))/1000, "%"
		An=A[:,(Neq*(2*H+1)+1)*n:(Neq*(2*H+1)+1)*(n+1)]
		Anw=np.zeros((An.shape[0],An.shape[1]*Nc ))
		for m in range(0,Nc): Anw[:, An.shape[1]*m:An.shape[1]*(m+1)]=np.array(np.array(An.subs('xi',nodes[m])), np.float)*weights[m]
		Anw[abs(Anw)<1e-7]=0##########################################
		for k in range(0,P+1):
			for i in range(k,P+1): #k
				for j in range(i,P+1): #i
					for m in range(0,Nc):
						Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]  + Anw[:, An.shape[1]*m:An.shape[1]*(m+1)]*PSI[i,m]*PSI[j,m]*PSI[k,m]
					if (k==i and i!=j) : 
						Q[j*An.shape[0]:(j+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ i):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ i+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)] 
						Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ k):An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ k+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]
					elif (k!=i and k!=j and i!=j):
						Q[j*An.shape[0]:(j+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ i):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ i+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]
						Q[k*An.shape[0]:(k+1)*An.shape[0] , An.shape[1]*(n*(P+1) + i*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + i*(P+1) *Neq*(2*H+1)+ j+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)] 
						Q[j*An.shape[0]:(j+1)*An.shape[0] , An.shape[1]*(n*(P+1) + i*(P+1) *Neq*(2*H+1)+ k):An.shape[1]*(n*(P+1) + i*(P+1) *Neq*(2*H+1)+ k+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)] 
						Q[k*An.shape[0]:(k+1)*An.shape[0] , An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ i):An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ i+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]
						Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ k):An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ k+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]
					elif (k!=i and i==j):
						Q[k*An.shape[0]:(k+1)*An.shape[0] , An.shape[1]*(n*(P+1) + i*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + i*(P+1) *Neq*(2*H+1)+ j+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)]
						Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ k):An.shape[1]*(n*(P+1) + j*(P+1) *Neq*(2*H+1)+ k+1)]=Q[i*An.shape[0]:(i+1)*An.shape[0] , An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j):An.shape[1]*(n*(P+1) + k*(P+1) *Neq*(2*H+1)+ j+1)] 
	
	Q=scipy.sparse.csr_matrix(Q)
	Qlist=[None]*Neq*(2*H+1)*(P+1)    # we want to store the bilinear application matrices in a list...it will be easy to operate with them
	for i in range(0,Neq*(2*H+1)*(P+1)): Qlist[i]=Q[:,i*Q.shape[1]/(Neq*(2*H+1)*(P+1)):(i+1)*Q.shape[1]/(Neq*(2*H+1)*(P+1))]
	ninc=(Neq*(2*H+1) +1)*(P+1)
	
	#### SINOU'S PHASE CONDITIONS---> P equations
	for p in range(1,P+1):
		q=scipy.sparse.lil_matrix((Qlist[0].shape))
		for i in range(0,(Neq*(2*H+1)+1)*(P+1)):
			U=np.matrix(np.zeros(( (Neq*(2*H+1) +1)*(P+1), 1)))
			U[i,0]=1
			for j in range(0,(Neq*(2*H+1)+1)*(P+1)):
				V=np.matrix(np.zeros(( (Neq*(2*H+1) +1)*(P+1), 1)))
				V[j,0]=1
				for h in range(1,H+1):
					q[i,j]= q[i,j]+np.dot( U[Neq*(1+(h-1)*2):Neq*(2+(h-1)*2),0].T,V[ninc/(P+1)*p+Neq*(2+(h-1)*2):ninc/(P+1)*p+Neq*(3+(h-1)*2),0])  - np.dot( U[Neq*(2+(h-1)*2):Neq*(3+(h-1)*2),0].T,V[ninc/(P+1)*p+Neq*(1+(h-1)*2):ninc/(P+1)*p+Neq*(2+(h-1)*2),0])
		q=scipy.sparse.csr_matrix(q)
		Qlist.append(q)
			
	print "Computation of stochastic quadratic operator finished"
	return Qlist

def get_Qbeta(U,V,Q):
	Qbeta=np.zeros((U.shape[0]-1,1))
	V=np.matrix(V)
	for i in range(0,len(Q)):
		Qbeta[i,0]=np.dot(U.T,Q[i]*V)
	return Qbeta

def get_R(Beta,L0,L,Q,P,Ut0):
	Qbeta=get_Qbeta(Beta,Beta,Q)
	R=L0 + np.dot(L,Beta) + Qbeta
	return R
	
def get_dRdB(Beta,H,Neq,ninc,neqsys,L0,L,Q,P,Ut0):
	dRdB = np.matrix(np.zeros((neqsys,ninc)))
	for c in range(0,ninc):
			Ue = np.zeros((ninc,1))
			Ue[c] = 1
			dRdB[:,c] = np.dot(L,Ue) + get_Qbeta(Ue,Beta,Q) +get_Qbeta(Beta,Ue,Q)
	return dRdB
	
def get_Ut(B,dRdB,ninc,randvect,chemin,P):
	import scipy
	from math import sqrt
	b =np.zeros((ninc,1))
	b[-1]=1
	Tmat = np.vstack((dRdB,np.hstack((randvect[:,:ninc/(P+1)],np.zeros((1,ninc/(P+1)*P)) )) ))
	Ut=-scipy.linalg.solve(Tmat,b)
	Ut=Ut/np.linalg.norm(Ut)
	return Ut
	
def get_UtANM(B,dRdB,ninc,randvect,chemin,P,Amax,Ut0):
	import scipy
	from math import sqrt
	b =np.zeros((ninc,1))
	b[-1]=1
	Tmat = np.vstack((dRdB,randvect))
	Ut=scipy.linalg.solve(Tmat,b)
	Ut=Ut/np.linalg.norm(Ut)
	return Ut

		
	
