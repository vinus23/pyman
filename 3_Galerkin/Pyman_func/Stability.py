import numpy as n
from sys import exit
from math import pi

def JQ(Ustab,Vstab,Gamma,M):
		dof=M.shape[0]
		
		Ustab=(n.matrix(Ustab)).T
		Vstab=(n.matrix(Vstab)).T
		Ustab=Ustab[0:dof,0]
		Vstab=Vstab[0:dof,0]
		u2=n.matrix(n.multiply(Ustab,Vstab))

		JQ=n.matrix(n.zeros((2*dof,2*dof)))+0J
		JQ3=3*n.linalg.inv(M)*(-Gamma*u2)
		JQ[dof:,0:dof]=JQ3
		return JQ


#def JL(Ustab):
		#JL=n.zeros((2,2))
		#return JL
#def J0(UU,M,C,K):
	#dof=M.shape[0]
	#J01=n.zeros((dof,dof))
	#J02=n.eye(dof)
	#J03=-n.dot(n.linalg.inv(M),K)
	#J04=-n.dot(n.linalg.inv(M),C)
	#J0=n.vstack(n.hstack(J01,J02),n.hastack(J03,J04))
	#J0=n.array([[0 ,1] , [-ome0**2, -mu]])
	#return J0

def Hill(ddlstab,nbddlstab,nbddl,HH,M,C,K,Gamma):
        II = n.eye(nbddl) 
        JJ = n.zeros((nbddl*(2*HH+1),nbddl)) +0j
        omega = ddlstab[-1]
        lambd = ddlstab[-2]
        UU = ddlstab[1 -1:nbddlstab]
        UH = ddlstab[nbddlstab+1 -1: - 2]
        HILL = n.zeros((  nbddl*(2*HH+1),nbddl*(2*HH+1) ))  + 0j
        dof=M.shape[0]
        J01=n.zeros((dof,dof))
        J02=n.eye(dof)
        J03=-n.dot(n.linalg.inv(M),K)
        J04=-n.dot(n.linalg.inv(M),C)
        J0=n.vstack((n.hstack((J01,J02)),n.hstack((J03,J04)) ))
        JL=n.zeros((2*dof,2*dof))
        for h in range(-HH,HH +1):
	            hm = abs(h) - 1
	            hp = abs(h)
	            
	            for p in range(h,HH +1):
		                pm = abs(p) - 1
		                pp = abs(p)
		                
		                if abs(p+h) < (HH+1):       
			                    ind = p+h + (HH+1)
			                    ind0 = ind - 1
			                    opp = -(p+h) + (HH+1)
			                    opp0 = opp - 1
			                    
			                    if h == 0:
										Ustab = n.hstack((UU,lambd))
			                    elif h < 0:
										Ustab = n.hstack((0.5*UH[nbddlstab*2*hm+1 -1:nbddlstab*(2*hp-1)], lambd)) + n.hstack(( 0.5j*UH[nbddlstab*(2*hm+1)+1 -1:nbddlstab*2*hp], 0 ))
			                    else:
										Ustab = n.hstack((0.5*UH[nbddlstab*2*hm+1 -1:nbddlstab*(2*hp-1)], lambd))  - n.hstack(( 0.5j*UH[nbddlstab*(2*hm+1)+1 -1:nbddlstab*2*hp], 0 ))
			                              
			                    if p == 0:
										Vstab = n.hstack((UU,lambd))
			                    elif p < 0:
										Vstab = n.hstack((0.5*UH[nbddlstab*2*pm+1 -1:nbddlstab*(2*pp-1)], lambd)) + n.hstack(( 0.5j*UH[nbddlstab*(2*pm+1)+1 -1:nbddlstab*2*pp], 0 ))
			                    else: Vstab = n.hstack((0.5*UH[nbddlstab*2*pm+1 -1:nbddlstab*(2*pp-1)], lambd)) -  n.hstack(( 0.5j*UH[nbddlstab*(2*pm+1)+1 -1:nbddlstab*2*pp], 0 ))
			                    
			                    if p == h: JJ[ind0*nbddl+1 -1:ind*nbddl,1 -1:nbddl] = JJ[ind0*nbddl+1 -1:ind*nbddl,1 -1:nbddl] + JQ(Ustab,Vstab,Gamma,M)
			                    else:
									JJ[ind0*nbddl+1 -1:ind*nbddl,1 -1:nbddl] = JJ[ind0*nbddl+1 -1:ind*nbddl,1 -1:nbddl] + JQ(Ustab,Vstab,Gamma,M)
									JJ[opp0*nbddl+1 -1:opp*nbddl,1 -1:nbddl] = JJ[opp0*nbddl+1 -1:opp*nbddl,1 -1:nbddl] + JQ(Ustab.conjugate(),Vstab.conjugate(),Gamma,M)

	            if h < 1:
					if h == 0:
							JJ[ (h+HH)*nbddl+1 -1: (h+HH+1)*nbddl,1 -1:nbddl] = JJ[ (h+HH)*nbddl+1 -1: (h+HH+1)*nbddl,1 -1:nbddl] + J0 + JL
					else:
							JJ[ (h+HH)*nbddl+1 -1: (h+HH+1)*nbddl,1 -1:nbddl] = JJ[ (h+HH)*nbddl+1 -1: (h+HH+1)*nbddl,1 -1:nbddl] + JL
							JJ[ (-h+HH)*nbddl+1 -1: (-h+HH+1)*nbddl,1 -1:nbddl] = JJ[ (-h+HH)*nbddl+1 -1: (-h+HH+1)*nbddl,1 -1:nbddl] + JL
		
        for g in range(1,HH+1):
				gm = g - 1
				go = 2*HH - gm
				HILL[1 -1:(HH+g)*nbddl,gm*nbddl+1 -1:g*nbddl] = JJ[(HH-gm)*nbddl+1 -1: ,:]
				HILL[gm*nbddl+1 -1:g*nbddl,gm*nbddl+1 -1:g*nbddl] = HILL[gm*nbddl+1 -1:g*nbddl,gm*nbddl+1 -1:g*nbddl] + 1j*omega*II*(HH - gm)
				HILL[(HH-gm)*nbddl+1 -1: ,go*nbddl+1 -1:(go+1)*nbddl] = JJ[1-1:(HH+g)*nbddl,:]
				HILL[go*nbddl+1 -1:(go+1)*nbddl,go*nbddl+1 -1:(go+1)*nbddl] = HILL[go*nbddl+1 -1:(go+1)*nbddl,go*nbddl+1 -1:(go+1)*nbddl] + 1j*omega*II*(gm - HH)
       
        HILL[:,HH*nbddl+1 -1:(HH+1)*nbddl] = JJ
       
        return HILL

def troncon_ddlstab(ordre,Ups,nbddlstab,varstab,NeqHH0,Amax,nbptstroncon,HH): #we are looking for the points we want to verify the stability...we use series calculated before to calculate the wanted values (varstab) at each troncon point
	import numpy
	coefstab = numpy.zeros((ordre+1,1))
	ddlstab = numpy.zeros((nbptstroncon,nbddlstab*(2*HH+1)+2))
	for i in range(1,nbddlstab +1):
			istab = varstab[i-1]
			for j in range(1,2*HH+1 +1):
			      for p in range(0,ordre +1):
						coefstab[p] = Ups[istab + (j-1)*NeqHH0 -1, ordre-p]
			      for p in range(1,nbptstroncon +1):
						a = Amax*(p-1) / (nbptstroncon-1)
						ddlstab[p -1,(i+(j-1)*nbddlstab) -1] = numpy.polyval(coefstab, a)
	for i in [1,2]:
		  istab = varstab[-1 - i]
		  for p in range (0,ordre +1):
				coefstab[p] = Ups[istab -1, ordre-p]
		  for p in range(1,nbptstroncon +1):
				a = Amax*(p-1) / (nbptstroncon-1) 
				ddlstab[p -1, -1 -(i-1)] = numpy.polyval(coefstab, a)
	return ddlstab
	
def stabF(nbptstroncon,nbddl,HH,ddlstab,nbddlstab,M,C,K,Gamma,tolstab):
    import numpy
    check = numpy.zeros((nbptstroncon,1))
    eigen = numpy.zeros((nbptstroncon,nbddl*(2*HH+1) )) +0J# all the alphas [debug]
    vectors = numpy.zeros((nbptstroncon,nbddl*nbddl*(2*HH+1) ))+0J
    XX=numpy.reshape(numpy.dot(numpy.ones((nbddl,1)),numpy.matrix(range(-HH,HH+1))), ( (2*HH+1)*nbddl,1 ) ,'F')

    for k in range(1,nbptstroncon +1):
		  omega=ddlstab[k -1, -1]
		  
		  H=Hill(ddlstab[k -1, :],nbddlstab,nbddl,HH,M,C,K,Gamma)
		  soluce,V = numpy.linalg.eig(H) 
		  soluce=soluce+ 0j
		  V=V+0J		  
		  med=(     numpy.sum(numpy.multiply(numpy.tile(XX,(1,nbddl*(2*HH+1)) ),abs(V)),axis=0)/numpy.sum(abs(V),axis=0)    )
		  ind=sorted(range(med.shape[1]), key=lambda k: abs(med[0,k]))
		  
		  test = 0
		  inc = 0
		  inc2T = numpy.zeros((nbddl))
		  for hh in range(1,nbddl +1):
			  inc = inc +1
			  if abs(abs((soluce[ind[inc - 1]]  ).imag   ) - omega/2) < 1e-4:
				  test = test + 1
				  if test == 1:
					  val0 = numpy.array(abs(   numpy.sign((soluce[ind[inc -1]]).imag) + numpy.sign((soluce[ind[inc -1]]).real)) )
					  inc2T[hh -1] = ind[inc -1]
				  elif   test == 2:
					  val1 = numpy.array(abs(    numpy.sign((soluce[ind[inc -1:inc+2]]).imag) + numpy.sign((soluce[ind[inc -1:inc+2]]).real)         ) )  -val0
					  indint = numpy.array((val1==0.1).nonzero()) - 1
					  if not indint.all():
						  inc2T[hh -1] = ind[inc -1+indint[0,0]]
						  inc = inc+2
						  test=2
					  else: inc2T[hh -1] = ind[inc+2 -1]
					   
			  else: inc2T[hh -1] = ind[inc -1]
		  
		  indorder = numpy.hstack((inc2T,numpy.setdiff1d(ind,inc2T)))
		  indorder = indorder.astype(int)
		  inc2T=inc2T.astype(int)
		  alf = soluce[inc2T]
		  #vecconv = V[:,inc2T]
		  rho=numpy.exp(alf*2*pi/ddlstab[k -1,-1])
		  
		  if any((abs(rho) > 1+tolstab)): stab=0 ## rho=alfa**2pi/angfreq
		  else: stab=1
		  
		  check[k -1,1 -1] = stab
		  eigen[k-1,:] = soluce[indorder]  # premieres 2 valeurs de chaque ligne (ind2T) et apres toutes les autres
		  #vectors[k-1,:]= (numpy.reshape(vecconv,(nbddl*nbddl*(2*HH+1),1),'F')).T
    
    return check, eigen  #,vectors
