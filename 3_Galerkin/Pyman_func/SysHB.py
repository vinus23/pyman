import numpy, LQdeterministic
numpy.set_printoptions(threshold=numpy.nan)
def get_R(U,H,Neq,M,K,C,Gamma,ff,Pert):
		L0=LQdeterministic.L0(H,Neq,ff)
		L=LQdeterministic.L(H,Neq,M,K,C,U)
		Q=LQdeterministic.Q(H,Neq,Gamma,U,U)
		R=L0+L+Q+Pert
		return R
		
def get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff):

		dRdU = numpy.zeros((neqsys,ninc))
		for c in range(1,ninc +1):
				Ue = numpy.zeros((ninc,1))
				Ue[c-1] = 1
				dRdU[:,c-1] = numpy.transpose(LQdeterministic.L(H,Neq,M,K,C,Ue) + LQdeterministic.Q(H,Neq,Gamma,Ue,U) +LQdeterministic.Q(H,Neq,Gamma,U,Ue))
		return dRdU
		
def  get_Ut(U,dRdU,ninc,randvect,chemin):
		import scipy
		from math import sqrt
		b =(numpy.zeros((ninc,1)))
		b[-1]=1

		Tmat = numpy.vstack((dRdU,randvect))
		
		Ut=-scipy.linalg.solve(Tmat,b)   
		
		chemin=numpy.matrix(chemin)
		uu=numpy.multiply(chemin,Ut)
		#print sqrt(numpy.dot (uu.T,Ut ))
		Ut = Ut *  1/sqrt(numpy.dot (uu.T,Ut ))	#normalisation
		
		return Ut 


