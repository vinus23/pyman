import numpy, scipy
from sys import exit
from time import clock

def correction(Beta,H,Neq,L0s,Ls,Qs,ninc,neqsys,Pert,randvect,chemin,seuilcorr,itemax,P,Ut0):
	import SysStoch
	from scipy import linalg
	nbites = 0
	
	R=SysStoch.get_R(Beta,L0s,Ls,Qs,P,Ut0)
	normR = numpy.linalg.norm(R)
	while normR>seuilcorr and nbites<itemax:
		if nbites==0:
			print "Correction started"
		nbites = nbites + 1
	
		dRdB=SysStoch.get_dRdB(Beta,H,Neq,ninc,neqsys,L0s,Ls,Qs,P,Ut0)
		Ut=SysStoch.get_Ut(Beta,dRdB,ninc,randvect,chemin,P)
		KK = numpy.vstack((dRdB,Ut.T))
		R1=numpy.vstack((R,0)) 
		Beta = Beta - scipy.linalg.solve(KK,R1)  
		R=SysStoch.get_R(Beta,L0s,Ls,Qs,P,Ut0)
		normR = numpy.linalg.norm(R)
		print "iter n.", nbites, "||R||", normR
		Ut0=Ut[:ninc/(P+1),:]		
	return Beta,Ut0

def correctionHB(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuilcorr,itemax,P):
	ninc=ninc/(P+1)
	neqsys=ninc-1
	import SysHB,sympy, numpy as np
	from scipy import linalg
	nbites = 0
	
	randvect=numpy.random.rand(1,ninc)
	chemin = numpy.ones((ninc,1))
	Pert = numpy.zeros((neqsys,1) )
	
	if type(M)!=type(Pert): M=np.array(np.array(M.subs('xi',0)), np.float)
	if type(K)!=type(Pert): K=np.array(np.array(K.subs('xi',0)), np.float) 
	if type(C)!=type(Pert): C=np.array(np.array(C.subs('xi',0)), np.float) 
	if type(Gamma)!=type(Pert): Gamma=np.array(np.array(Gamma.subs('xi',0)), np.float) 
	if type(ff)!=type(Pert): ff=np.array(np.array(ff.subs('xi',0)), np.float) 
	
	R=SysHB.get_R(U,H,Neq,M,K,C,Gamma,ff,Pert)

	normR = numpy.linalg.norm(R)
	while normR>seuilcorr and nbites<itemax:
				if nbites==0:
					print "Correction started HB"
				nbites = nbites + 1

				dRdU=SysHB.get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff)
							
				Ut=SysHB.get_Ut(U,dRdU,ninc,randvect,chemin)
				KK = numpy.vstack((dRdU,Ut.T))
				R1=numpy.vstack((R,0)) 
				U = U - scipy.linalg.solve(KK,R1)   #UUU=numpy.linalg.solve(K,R1)	
				R=SysHB.get_R(U,H,Neq,M,K,C,Gamma,ff,Pert)
				normR = numpy.linalg.norm(R)
				print "iter n.", nbites, "||R||", normR
	Ut=SysHB.get_Ut(U,SysHB.get_dRdU(U,H,Neq,ninc,neqsys,M,K,C,Gamma,ff),ninc,randvect,chemin)		
	return U,Ut
