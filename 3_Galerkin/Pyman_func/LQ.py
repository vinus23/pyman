import numpy,sympy
from sys import exit
def pL(U,M,K,C,Neq):
	pL=sympy.Matrix(numpy.zeros((Neq,1)))
	U=sympy.Matrix(U)
	pL[0:Neq/3,0]=  U[Neq/3:Neq/3*2,0]
	pL[Neq/3:Neq/3*2,0]= numpy.linalg.inv(M)*( -C*U[Neq/3:Neq/3*2,0] -K*U[0:Neq/3,0])
	pL[Neq/3*2:,0]=U[Neq/3*2:Neq,0]
	
	return pL
 
def pQ(U,V,gamma,Neq):
	pQ=sympy.Matrix(numpy.zeros((Neq,1)))
	U=sympy.Matrix(U)
	V=sympy.Matrix(V)

	if U.shape[0] !=Neq:
		U=U.T
		V=V.T
	
	pQ[Neq/3:Neq/3*2,0]= -gamma*numpy.matrix(numpy.multiply(U[0:Neq/3,0],V[Neq/3*2:Neq,0]))	
	pQ[Neq/3*2:,0]= -numpy.matrix(numpy.multiply(U[0:Neq/3,0],V[0:Neq/3,0]))
	return pQ
  
def pM(U,Neq):
	pM=sympy.Matrix(numpy.zeros((Neq,1)))
	U=sympy.Matrix(U)
	if U.shape[0] !=Neq: U=U.T
	pM[0:Neq/3*2,0]=U[0:Neq/3*2,0]
	return pM

def L0(H,Neq,FF):
	L0=sympy.Matrix(numpy.zeros((Neq*(2*H+1),1)))
	FF=sympy.Matrix(FF)
	L0[Neq+Neq/3:Neq+Neq/3*2,0]=FF
	return L0
	
def L(H,Neq,M,K,C,U):
	indc=numpy.arange(Neq+1,Neq*(2*H-1)+1   +1,2*Neq)  #index des petits vecteurs  uic 
	inds=numpy.arange(2*Neq+1,Neq*(2*H)+1   +1,2*Neq)    #index des petits vecteurs  uis 
	
	L=sympy.Matrix(numpy.zeros((Neq*(2*H+1),1)))
	L[:Neq,0]=pL(U,M,K,C,Neq)
	
	for i in range(1,H +1):
		L[Neq+2*(i-1)*Neq+1 -1:2*Neq+2*(i-1)*Neq,0]  =pL(U[indc[i -1] -1:indc[i -1]+Neq-1,0],M,K,C,Neq)
		L[2*Neq+2*(i-1)*Neq+1 -1:3*Neq+2*(i-1)*Neq,0]=pL(U[inds[i -1] -1:inds[i -1]+Neq-1,0],M,K,C,Neq)
	
	return L

def Q(H,Neq,gamma,U,V):
	indc=numpy.arange(Neq+1,Neq*(2*H-1)+1   +1,2*Neq)  #index des petits vecteurs  uic 
	inds=numpy.arange(2*Neq+1,Neq*(2*H)+1   +1,2*Neq)    #index des petits vecteurs  uis 
	
	Q=sympy.Matrix(numpy.zeros((Neq*(2*H+1),1)))
	
	SOM=sympy.Matrix(numpy.zeros((Neq,1)))
	
	for j in range(1,H +1):   #harmonic 0
		SOM= SOM + pQ(U[indc[j -1] -1:indc[j-1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,Neq) + pQ(U[inds[j -1] -1:inds[j-1]+Neq-1],V[inds[j -1] -1:inds[j-1]+Neq-1],gamma,Neq)
	
	
	Q[:Neq,0]= 0.5*SOM + pQ(U[:Neq],V[:Neq],gamma,Neq) 
		
	for ih in range(1,H +1):   #other harmonics
		SOMC=sympy.Matrix(numpy.zeros((Neq,1)))
		SOMS=sympy.Matrix(numpy.zeros((Neq,1)))
		
		for j in range(1,ih):
			SOMC=SOMC +pQ(U[indc[ih-j -1] -1:indc[ih-j -1]+Neq-1],V[indc[j -1] -1:indc[j -1]+Neq-1],gamma,Neq) -pQ(U[inds[ih-j -1] -1:inds[ih-j-1]+Neq-1],V[inds[j -1] -1:inds[j-1]+Neq-1],gamma,Neq)
			SOMS=SOMS +pQ(U[indc[ih-j -1] -1:indc[ih-j -1]+Neq-1],V[inds[j -1] -1:inds[j -1]+Neq-1],gamma,Neq) +pQ(U[inds[ih-j -1] -1:inds[ih-j-1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,Neq)
   	
		for j in range(ih+1,H +1):
			SOMC=SOMC+ pQ(U[indc[j -1] -1:indc[j-1]+Neq-1],V[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],gamma,Neq)+pQ(U[inds[j-1] -1:inds[j-1]+Neq-1],V[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],gamma,Neq) +pQ(U[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,Neq)  +pQ(U[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],V[inds[j-1] -1:inds[j-1]+Neq-1],gamma,Neq)
			SOMS=SOMS+ pQ(U[inds[j -1] -1:inds[j-1]+Neq-1],V[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],gamma,Neq)-  pQ(U[indc[j-1] -1:indc[j-1]+Neq-1],V[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],gamma,Neq) -pQ(U[inds[j-ih -1] -1:inds[j-ih -1]+Neq-1],V[indc[j -1] -1:indc[j-1]+Neq-1],gamma,Neq)  +pQ(U[indc[j-ih -1] -1:indc[j-ih -1]+Neq-1],V[inds[j-1] -1:inds[j-1]+Neq-1],gamma,Neq)
		
		Q[indc[ih -1] -1:indc[ih -1]+Neq-1,0]=0.5*SOMC + pQ(U[indc[ih -1] -1:indc[ih -1]+Neq-1],V[:Neq],gamma,Neq)  +pQ(U[:Neq],V[indc[ih -1] -1:indc[ih -1]+Neq-1],gamma,Neq) - ih*V[Neq*(2*H+1)+1 -1]*pM(U[inds[ih -1] -1:inds[ih -1]+Neq-1],Neq)
		Q[inds[ih -1] -1:inds[ih -1]+Neq-1,0]=0.5*SOMS + pQ(U[inds[ih -1] -1:inds[ih -1]+Neq-1],V[:Neq],gamma,Neq)  +pQ(U[:Neq],V[inds[ih -1] -1:inds[ih -1]+Neq-1],gamma,Neq) + ih*V[Neq*(2*H+1)+1 -1]*pM(U[indc[ih -1] -1:indc[ih -1]+Neq-1],Neq)
	
	return Q
