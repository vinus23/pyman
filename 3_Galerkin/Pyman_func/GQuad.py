from openturns import *
import numpy

def collocation(Nc,family):
	if family=="hermite":
		hermite = HermiteFactory()
		weights = NumericalPoint()
		nodes = hermite.getNodesAndWeights(Nc, weights)

	return nodes,weights

def get_PSI(nodes,P,family):
	PSI=numpy.zeros(( P+1,len(nodes) ))
	if family=="hermite":
		from numpy.polynomial.hermite_e import hermeval
		for i in range(0,len(nodes)):
			node=nodes[i]
			for j in range(0,P+1): PSI[j,i]=hermeval(node,numpy.hstack((numpy.zeros(j),1)))
	return PSI

def get_normfact(P):
	import math
	normfact=numpy.zeros((P+1,1))
	for i in range(0,P+1):
		normfact[i]=math.factorial(i)
	return normfact
