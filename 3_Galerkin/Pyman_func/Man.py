import numpy, time
from sys import exit
import matplotlib.pyplot as plt
from math import pi
numpy.set_printoptions(threshold=numpy.nan)

def get_Us(ninc,nbptstroncon,Ups,Amax):
	Us = numpy.zeros((ninc,nbptstroncon))
	coefsUs = Ups[:, ::-1]
	ass=Amax*numpy.arange(0,nbptstroncon)*(1.0/(nbptstroncon-1))
	for i in range(0,ninc): Us[i,:] = numpy.polyval(coefsUs[i,:], ass)
	return Us
def get_Ustab(check,eigen):
	Ustab=numpy.vstack(( (check.T).squeeze(),eigen.T))
	return Ustab

def Branche(Beta,Ut,Ut0,nt,H,Neq,P,ninc,neqsys,M,K,C,Gamma,ff,L0s,Ls,Qs,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,f):
	import numpy as np
	if type(M)!=type(Pert): M=np.array(np.array(M.subs('xi',0)), np.float)
	if type(K)!=type(Pert): K=np.array(np.array(K.subs('xi',0)), np.float) 
	if type(C)!=type(Pert): C=np.array(np.array(C.subs('xi',0)), np.float) 
	if type(Gamma)!=type(Pert): Gamma=np.array(np.array(Gamma.subs('xi',0)), np.float) 
	if type(ff)!=type(Pert): ff=np.array(np.array(ff.subs('xi',0)), np.float) 
	import Preliminar, SysStoch,Series,Plotting,Stability
	from matplotlib.pyplot import show

	
	for i in range(1,nt +1):
		
			#if stepcorr==1 and i!=1:  Beta,Ut0=Preliminar.correction(Beta,H,Neq,L0s,Ls,Qs,ninc,neqsys,Pert,randvect,chemin,seuilcorr,itemax,P,Ut0)
			
			Ups,Amax=Series.calculate_series(Beta,Ut,H,Neq,ninc,neqsys,L0s,Ls,Qs,randvect,chemin,ordre,P,Ut0,M,K,C,ff,Gamma,seuil,i)
			Ups=numpy.array(Ups)
					
			Beta=Series.eval_serie(ordre,ninc,Amax,Ups)
			Ut=Series.tangente_serie(ordre,ninc,Amax,Ups)
			Ut0=numpy.zeros((ninc/(P+1),1)) #not used
			Ut0[-1]=1 #not used
			R=SysStoch.get_R(Beta,L0s,Ls,Qs,P,Ut0)
			
			
			

			Betasol=get_Us(ninc,nbptstroncon,Ups,Amax)
			numpy.save(f,Betasol)

			lastfreq=Beta[ninc/(P+1)-1,0]
			print "Tr.", i, "finished  --  ||R||", float(int(numpy.linalg.norm(R)*1e10))/1e10, "Amax", float(int(Amax*1000))/1000, "Last freq" , float(int(lastfreq/2.0/pi*100))/100
			if lastfreq>=finalfreq*2*pi: break
	f.close()
				

