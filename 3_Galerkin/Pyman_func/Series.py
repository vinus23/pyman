import numpy
#from sys import exit
numpy.set_printoptions(threshold=numpy.nan)
def calculate_series(Beta,Ut,H,Neq,ninc,neqsys,L0s,Ls,Qs,randvect,chemin,ordre,P,Ut0,M,K,C,ff,Gamma,seuil,nnn):
		import LQdeterministic, scipy, SysStoch,SysHB
		pmax=ordre
		Utold=Ut
		#U1=Ut0
		
		####Calculate the next deterministic tangent and the path-parameter Amax NOT IMPORTANT IN THIS CASE, WE USE 	ANOTHER VECTOR FOR THE ORTHOGONALITY
		#dRdU=SysHB.get_dRdU(Beta[:ninc/(P+1),:],H,Neq,ninc/(P+1),(neqsys-P)/(P+1),M,K,C,Gamma,ff)
		#U1=SysHB.get_Ut(Beta[:ninc/(P+1),:],dRdU,ninc/(P+1),randvect[:,:ninc/(P+1)],chemin[:ninc/(P+1),:])
		#if numpy.dot(U1.T,Ut0)<0: U1=-U1
		#KK = numpy.vstack((dRdU,numpy.transpose(U1)))
		#Ups = numpy.zeros((ninc/(P+1),pmax+1))
		#Ups[:,0] = numpy.transpose(Beta[:ninc/(P+1),:])
		#Ups[:,1] = numpy.transpose(U1) 
		#lu=scipy.linalg.lu_factor(KK)
		#for p in range(2,pmax +1):
				#Fpnl = numpy.zeros((ninc/(P+1),1))
				#for r in range(1,p-1 +1): Fpnl = Fpnl - numpy.vstack((  LQdeterministic.Q(H,Neq,Gamma,Ups[:,r],Ups[:,p-r]),  [0]))  
				#Ups[:,p]=(scipy.linalg.lu_solve(lu,Fpnl)).T
		#Ups0=Ups
		
		############################################################		
		#Calculate the ANM coefficients for the stochastic system

		dRdB=SysStoch.get_dRdB(Beta,H,Neq,ninc,neqsys,L0s,Ls,Qs,P,Ut0)
		U1=SysStoch.get_UtANM(Beta,dRdB,ninc,randvect,chemin,P,1,Ut0)
		
		if  numpy.dot(U1.T,Ut)<0 : U1=-U1
		#if  numpy.dot(U1[ninc/(P+1):ninc/(P+1)*2,:].T,Utold[ninc/(P+1):ninc/(P+1)*2,:])<0: U1[ninc/(P+1):ninc/(P+1)*2,:]=-U1[ninc/(P+1):ninc/(P+1)*2,:]
		#if  numpy.dot(U1[ninc/(P+1)*2:ninc/(P+1)*3,:].T,Utold[ninc/(P+1)*2:ninc/(P+1)*3,:])<0: U1[ninc/(P+1)*2:ninc/(P+1)*3,:]=-U1[ninc/(P+1)*2:ninc/(P+1)*3,:]
		#if  numpy.dot(U1[ninc/(P+1)*3:ninc/(P+1)*4,:].T,Utold[ninc/(P+1)*3:ninc/(P+1)*4,:])<0: U1[ninc/(P+1)*3:ninc/(P+1)*4,:]=-U1[ninc/(P+1)*3:ninc/(P+1)*4,:]
																									
		KK = numpy.vstack((dRdB,numpy.transpose(U1*chemin)))   ###
		
		Ups = numpy.matrix(numpy.zeros((ninc,pmax+1)))
		Ups[:,0] = (Beta) 
		Ups[:,1] = (U1) 
		lu=scipy.linalg.lu_factor(KK)
		for p in range(2,pmax +1):
				Fpnl = numpy.zeros((ninc,1))
				for r in range(1,p-1 +1): Fpnl = Fpnl - numpy.vstack((  SysStoch.get_Qbeta(Ups[:,r],Ups[:,p-r],Qs),  [0]))
				Ups[:,p]=(scipy.linalg.lu_solve(lu,Fpnl))
		FpnlA = numpy.zeros((neqsys,1))
		for r in range (1,p +1): FpnlA=FpnlA - SysStoch.get_Qbeta(Ups[:,r],Ups[:,p-r+1],Qs)
		if numpy.linalg.norm(FpnlA) == 0: Amax = 10
		else:  Amax = (seuil/numpy.linalg.norm(FpnlA))**(1.0/(ordre+1))
		
		
		#Ut=tangente_serie(ordre,ninc/(P+1),Amax,Ups0)
		#Ut0new=Ut
		return Ups, Amax

def eval_serie(ordre,ninc,Amax,Ups):   #Evaluation de la series Ups a l'ordre
  coefs = numpy.zeros((ordre+1,1))
  U = numpy.zeros((ninc,1))
  ass = numpy.zeros((ordre+1, 1)) #Creation de la liste des coef dans le bon ordre
  t = 1
  for p in range(1,ordre+1 +1):
	  ass[p -1,0] = t
	  t = t * Amax
  U = numpy.dot(Ups,ass)
  return U

def tangente_serie(ordre,ninc,Amax,Ups):
	Ut = numpy.zeros((ninc, 1))
	for i in range(1,ordre-1 +1):
		Ut = Ut + i * Amax**(i-1) * numpy.reshape(Ups[:,i],(ninc,1),'F')
	return Ut
