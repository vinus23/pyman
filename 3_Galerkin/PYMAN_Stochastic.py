###ANM-PCE-Intrusive Galerkin
import os, sys,time
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Pyman_func" )
import random,numpy,SysHB,Preliminar,Man,sympy,GQuad,SysStoch
from sys import exit
###
H=3       	#nb of harmonics
P=3       #PCE basis order
dof=2	   #degrees of freedom
Nc=7     # number of Gaussian quadrature points to calculate the integral (scalar product)
###Physical Parameters, matrices M,C,K,Gamma(nonlinear coeff.) and vector of Forces
xi=sympy.symbols('xi') #random variable (gaussian)
m=1.0
k=15000.0
c=1.0
knl=5.0e10*(1+0.1*xi)
K=numpy.array([[2*k,-k],[-k,k]])
C=numpy.array([[2*c,-c],[-c,c]])
M=numpy.array([[m,0],[0,m]])
Gamma=sympy.Matrix(numpy.eye(dof)*0)
Gamma[0,0]=knl
ff=numpy.zeros((dof,1))
ff[0,0]=1		#Put a force on one or more dof
###
Neq=3*dof #n of equations
ninc=(Neq*(2*H+1))* (P+1)  +P+1
neqsys=ninc-1
###
finalfreq=50  #in Hz, where we want to arrive
Hdisp=[1]  #harmonics we want to display
ukvar=[2]  #uknown we want to dispay, (same size for Hdisp and ukvar!!!)
### 
randvect=numpy.random.rand(1,ninc)
###ANM parameters
chemin = numpy.ones((ninc,1)) # default path vector (weighting function)
ordre = 22;            					#default ANM series order
nbptstroncon = 5    					#default sampling of portions for display
seuil = 1.0e-8           					#default ANM threshold
itemax = 20           					#default max. iteration for N-R correction
seuilcorr = 1.0e-8      				#default threshold for N-R correction
stepcorr=0    							#put 1 to correct at each step
ntronc=500   						#Nb of steps MAX
###
if os.path.exists((os.path.dirname(os.path.realpath(__file__)))+"/Data/"+"values"): os.remove((os.path.dirname(os.path.realpath(__file__)))+"/Data/"+"values")
###Perturbation
cfp = 0.0            #switch to 1 (or -1) if you want to perturb
Pertampl=1.0e-6
Fp = numpy.random.rand(neqsys,1) # perturbation vector
Fp = Fp/numpy.linalg.norm(Fp) # normalization
Pert=cfp*Pertampl*Fp
###Stability
SS=0    			    #put to 1 if you want the stability analysis
varstab = numpy.hstack(( numpy.arange(1,dof+1) , numpy.array([Neq*(2*H+1)+1, Neq*(2*H+1)+1, H]) )) #Variables used for stability analysis: Dispacements, frequency 
tolstab = 1e-4    #tol used in stability analysis

###Initial values
Beta=numpy.zeros(( ninc,1 ))
Beta[ninc/(P+1)-1,:]=0.01

####################################################################################################################################################
###Find the applications L0,L,Q for the stochastic problem
nodes,weights=GQuad.collocation(Nc,"hermite") ### for integration

Beta0,Ut0=Preliminar.correctionHB(Beta[:ninc/(P+1),:],H,Neq,ninc,neqsys,M,K,C,Gamma,ff,Pert,randvect,chemin,seuilcorr,itemax,P)
L0s=SysStoch.L0(P,nodes,weights,H,Neq,ff)  # L0 stochastic
Ls=SysStoch.L(P,nodes,weights,Neq,H,M,K,C)     #Matrix such that L(Beta)=Lstoch*Beta
Qs=SysStoch.Q(P,nodes,weights,H,Neq,Gamma) #List of quadratic operator matrices

###STARTING###
Ut0=numpy.zeros((ninc/(P+1),1))
Ut0[-1]=1
Beta[:ninc/(P+1),:]=Beta0
Beta,Ut00=Preliminar.correction(Beta,H,Neq,L0s,Ls,Qs,ninc,neqsys,Pert,randvect,chemin,seuilcorr,itemax,P,Ut0)
Ut=SysStoch.get_Ut(Beta,SysStoch.get_dRdB(Beta,H,Neq,ninc,neqsys,L0s,Ls,Qs,P,Ut0),ninc,randvect,chemin,P)

obFile = open(os.path.dirname(os.path.realpath(__file__))+"/Data/"+"values","ab")
if Ut0[ninc/(P+1)-1,0]<0: Ut0=-Ut0
Man.Branche(Beta,-Ut,Ut0,ntronc ,H,Neq,P,ninc,neqsys,M,K,C,Gamma,ff,L0s,Ls,Qs,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,obFile)
