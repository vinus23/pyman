import numpy as np, os, matplotlib.pyplot as plt
from sys import exit
from math import pi
from numpy.polynomial.hermite_e import hermeval
from openturns import *
filename="values"
dof=2
Neq=dof*3
H=3
hdisp=1
udisp=1
P=3
nbptstroncon=5
Ncourves=100
RandomGenerator.SetSeed(0)
RandomExp1 = MonteCarloExperiment(Normal(0,1),Ncourves) #generate a samplelike for the MCS...it's useful to compare PCE to MCS
z=RandomExp1 .generate( )   #random variable

MAX=500
start=0 #start from ANM iteration number....
#################################################################################################################
nincHB=(Neq*(2*H+1)) + 1
Icos=Neq+udisp+(hdisp-1)*2*Neq
fp = open(os.path.dirname(os.path.realpath(__file__))+"/Data/"+filename,"rb")

psi=np.zeros((P+1,Ncourves))
for l in range(0,Ncourves):
	for o in range(0,P+1): psi[o,l]=hermeval(z[l],np.hstack((np.zeros(o),1)))
psi=psi.T

for j in range (0,start): Us=np.load(fp)
for tr in range(start,MAX):
	print "TR.", tr+1
	Betacos=np.zeros((P+1,nbptstroncon))
	Betasin=np.zeros((P+1,nbptstroncon))
	Betafreq=np.zeros((P+1,nbptstroncon))
	Us=np.load(fp)
	Amp=np.zeros((Ncourves, nbptstroncon))
	Freq=np.zeros((Ncourves, nbptstroncon))
	
	for i in range(0,P+1): Betafreq[i]=Us[nincHB*(i+1)-1,:]
	for i in range(0,P+1): Betacos[i,:]=Us[Icos +nincHB*i -1,:]
	for i in range(0,P+1): Betasin[i,:]=Us[Icos+Neq +nincHB*i -1,:]
	
	Freq=np.dot(psi,Betafreq)
	Betacos=np.dot(psi,Betacos)
	Betasin=np.dot(psi,Betasin)
	Amp=(Betacos**2+Betasin**2)**0.5
	Freq=np.dot(psi,Betafreq)
	
	for l in range(0,Ncourves):
		#col='b'
		#if tr/2==tr/2.0: col='y'
		plt.figure(1)
		plt.plot(Freq[l,:]/2.0/pi,Amp[l,:],'g')
	plt.plot(Betafreq[0,:]/2.0/pi,(Betacos[0,:]**2+Betasin[0,:]**2)**0.5,'r',linewidth=2)
	stdf=np.std(Freq,axis=0)/2.0/pi
	stda=np.std(Amp,axis=0)
	plt.figure(2)
	plt.plot(Betafreq[0,:]/2.0/pi,(Betacos[0,:]**2+Betasin[0,:]**2)**0.5,'g',linewidth=2)
	plt.plot(Betafreq[0,:]/2.0/pi+stdf,(Betacos[0,:]**2+Betasin[0,:]**2)**0.5+stda,'c',linewidth=1.5)
	plt.plot(Betafreq[0,:]/2.0/pi-stdf,(Betacos[0,:]**2+Betasin[0,:]**2)**0.5-stda,'c',linewidth=1.5)
	if fp.tell() == os.fstat(fp.fileno()).st_size: break

mean=np.loadtxt((os.path.dirname(os.path.realpath(__file__)))+"/mean.txt" )
std=np.loadtxt((os.path.dirname(os.path.realpath(__file__)))+"/std.txt" )

plt.plot(mean[0],mean[1],'y--')
plt.plot(mean[0]+std[0],mean[1]+std[1],'m--')
plt.plot(mean[0]-std[0],mean[1]-std[1],'m--')

fp.close()
for i in range(1,2 +1):
	plt.figure(i)
	plt.yscale('log')
	plt.xlim((0,55))
	#plt.ylim((1e-7,1e-2))

plt.show()
exit()
