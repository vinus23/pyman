import numpy, time,os
from sys import exit
from math import pi
import matplotlib.pyplot as plt
numpy.set_printoptions(threshold=numpy.nan)

def get_Us(ninc,nbptstroncon,Ups,Amax,Neq,ukvar,Hdisp):
	Us = numpy.zeros(( 2*len(ukvar)+1  ,nbptstroncon))
	coefsUs = Ups[:, ::-1]
	ass=Amax*numpy.arange(0,nbptstroncon)*(1.0/(nbptstroncon-1))
	
	for i in range(0,len(ukvar)):
		idisp=ukvar[i]
		hdisp=Hdisp[i]
		Icos=Neq+idisp+(hdisp-1)*2*Neq #index of cosinus harmonic (matlab like)
		Isin=Icos+Neq
		Us[i,:]=numpy.polyval(coefsUs[Icos-1,:], ass)
		Us[i+len(ukvar)]=numpy.polyval(coefsUs[Isin-1,:], ass)
		#Amp=( (Hcos)**2+(Hsin)**2)**(0.5) #  sqrt(Hcos**2+Hsin**2)
		#Us[i,:]=Amp
	
	Us[-1,:] = numpy.polyval(coefsUs[-1,:], ass)
	return Us
	
def get_Ustab(check,eigen):
	Ustab=numpy.vstack(( (check.T).squeeze(),eigen.T))
	return Ustab

def Branche(U,Ut,nt,H,Neq,ninc,neqsys,invM,K,C,Gamma,ff,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,f,fileA,Nc):
	import Preliminar, SysHB,Plotting,Series,Plotting,Stability
	from matplotlib.pyplot import show
	
	Aold=numpy.zeros(Nc)
	vectfor=numpy.arange(Nc)
	
	for i in range(1,nt +1):
		
		Ups=[None]*Nc
		Fpnla=[None]*Nc
		AAA=numpy.zeros(Nc)
		#calculate series coefficients for each coll. point
		for ii in vectfor:
			Ups[ii],Fpnla[ii]=Series.calculate_series(U[:,:,ii],Ut[:,:,ii],H,Neq,ninc,neqsys,invM[:,:,ii],K[:,:,ii],C[:,:,ii],Gamma[:,:,ii],ff[:,:,ii],randvect,chemin,ordre)
			if numpy.linalg.norm(Fpnla[ii]) == 0: Amax = 1.0
			else: Amax = (seuil/numpy.linalg.norm(Fpnla[ii]))**(1.0/(ordre+1))
			AAA[ii]=Amax

		#follow the path and find the next point using series coefficients
		for ii in vectfor: 
			U[:,:,ii]=Series.eval_serie(ordre,ninc,AAA[ii],Ups[ii])
			Ut[:,:,ii]=Series.tangente_serie(ordre,ninc,AAA[ii],Ups[ii])	
			R=SysHB.get_R(U[:,:,ii],H,Neq,invM[:,:,ii],K[:,:,ii],C[:,:,ii],Gamma[:,:,ii],ff[:,:,ii],Pert)
			print "Tr.", i, "Node", ii+1, "finished  --  ||R||", numpy.linalg.norm(R)		
		
		#store the calculated tranch (for each coll.point)
		Avect=numpy.zeros((Nc,nbptstroncon))
		for ii in vectfor:
			Us=get_Us(ninc,nbptstroncon,Ups[ii],AAA[ii],Neq,ukvar,Hdisp)
			Avect[ii,:]=AAA[ii]*numpy.arange(0,nbptstroncon)*(1.0/(nbptstroncon-1))+ Aold[ii]
			numpy.savetxt(f[ii],Us)
			numpy.savetxt(fileA[ii],Avect[ii,:])
		
		Aold=Avect[:,-1]
		
		#if we arrive at the last frequency we want, the cycle is broken
		lastfreq=U[-1,0,:]/2.0/pi
		vectfor=numpy.arange(Nc)
		for ii in vectfor[::-1]:
			if lastfreq[ii]>=finalfreq: vectfor = numpy.delete(vectfor,ii)
		
		print lastfreq, vectfor
		if len(vectfor)==0: break

	
	#close the files
	for ii in range(0,Nc): f[ii].close()
	for ii in range(0,Nc): fileA[ii].close()
