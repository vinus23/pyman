import numpy as np, PYMAN, os, sys, matplotlib.pyplot as plt,glob
from sys import exit
from math import pi, factorial
from openturns import *
from numpy.polynomial.hermite_e import hermeval
from numpy.polynomial.legendre import legval

folder="/Data_D"
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+folder)
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Cubature")
import GQuad,Smolyak,Multivariate
RandomGenerator.SetSeed(0)
#################################################################################################################################################
DIM=2
deg=3
family=["hermite","legendre","legendre"]
Ncourves=1000 #nb of courves we want to get
#################################################################################################################################################
dist=[None]*DIM
xi=[None]*DIM
for i in range(0,DIM):
	if family[i]=="legendre":dist1=Uniform(-1,1)
	if family[i]=="hermite": dist1=Normal(0,1)
	RandomExp1 = MonteCarloExperiment(dist1,Ncourves) #generate a samplelike for the MCS...it's useful to compare PCE to MCS
	xi[i]=RandomExp1 .generate( )   #random variable

P=factorial(deg+DIM)/factorial(deg)/factorial(DIM)-1
if DIM==1:
	Nc=2*deg+1
	Nc=9
	nodes,weights=GQuad.collocation(Nc,family[0])
	W=np.matrix(weights).T
	normfact=GQuad.get_normfact(P,family[0])
	PSI=GQuad.get_PSI(nodes,P,family[0])
elif DIM>1:
	lev=2
	nodes,weights=Smolyak.grid(DIM,lev,family)
	Nc=nodes.shape[1]
	PSI,normfact=Multivariate.get_multivar(DIM,deg,P,nodes,family)

#DOFs
dof=2
Dofs=[0,1]

#last frequency we want to arrive at
lastfreq=55   #in Hz
#percentile to plot
percen=95

#### 1  Reading the files in which the output is stored
Ntr=np.zeros(Nc)
for i in range(0,Nc): Ntr[i]=(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1)).shape[0]/(dof*2+1))
nbptstroncon =np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(1)).shape[1]
Ntr=Ntr.astype(int)

Freq=[None]*Nc
Hcos=[None]*len(Dofs)*Nc
Hsin=[None]*len(Dofs)*Nc
Amp=[None]*len(Dofs)*Nc
aa=[None]*Nc
finalA=np.zeros(Nc)

for i in range(0,Nc):
	Freq[i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1))[dof*2::dof*2+1,:]/2.0/pi, (Ntr[i]*nbptstroncon,1))[:,0]
	aa[i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/A"+str(i+1)), (Ntr[i]*nbptstroncon,1))[:,0]
	finalA[i]=aa[i][-1]
	for j in Dofs:
		Hcos[Nc*j+i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1))[j::dof*2+1,:], (Ntr[i]*nbptstroncon,1))[:,0]
		Hsin[Nc*j+i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1))[j+dof::dof*2+1,:], (Ntr[i]*nbptstroncon,1))[:,0]
		#Amp[Nc*j+i]=(Hcos[Nc*j+i]**2+Hsin[Nc*j+i]**2)**0.5

finalA=min(finalA)

AA=np.arange(0,finalA,finalA/(max(Ntr)*nbptstroncon)/3)
for i in range(0,Nc):
	Freq[i]=np.interp(AA,aa[i],Freq[i])
	for j in Dofs: 
		Hcos[Nc*j+i]=np.interp(AA,aa[i],Hcos[Nc*j+i])
		Hsin[Nc*j+i]=np.interp(AA,aa[i],Hsin[Nc*j+i])
		Amp[Nc*j+i]=(Hcos[Nc*j+i]**2+Hsin[Nc*j+i]**2)**0.5
		plt.figure(j)
		plt.yscale('log')
		#plt.plot(Freq[i],(Hcos[Nc*j+i]**2+Hsin[Nc*j+i]**2)**0.5,'g')

#### 2 Calculate the stochastic modes (peak by peak)
colstd='Purple'
colper='GreenYellow'

if DIM==1:
	psi=np.zeros((P+1,Ncourves))
	for l in range(0,Ncourves):
		for p in range(0,P+1): 
			if family[0]=="hermite": psi[p,l]=hermeval(xi[0][l],np.hstack((np.zeros(p),1)))
			if family[0]=="legendre": psi[p,l]=legval(xi[0][l],np.hstack((np.zeros(p),1)))
elif DIM>1:
	randpoints=np.zeros((DIM,Ncourves))
	for i in range(0,DIM):
		for l in range(0,Ncourves): randpoints[i,l]=np.array(xi[i][l])[0]
	psi,ghost=Multivariate.get_multivar(DIM,deg,P,randpoints,family)


maxmin=[None]*Nc*len(Dofs)
for j in Dofs:
	for N in range(0,Nc): 
		maxmin[Nc*j+N]=[len(AA)-1]
		print maxmin[Nc*j+N]

plotlegend=[None]*len(Dofs)
for j in Dofs:
	print "Dof",j+1
	ind=np.zeros(Nc)
	meanfreq=np.zeros(0)
	meanamp=np.zeros(0)
	stdfreq=np.zeros(0)
	stdamp=np.zeros(0)
	perfreqbot=np.zeros(0)
	perfrequp=np.zeros(0)
	perampbot=np.zeros(0)
	perampup=np.zeros(0)
	for r in range(0,len(maxmin[j*Nc])	):
		#print r
		nstep=int((int(maxmin[j*Nc][r])-ind[0]+1)*1)
		freq=np.zeros((Nc,nstep))
		amp=np.zeros((Nc,nstep))
		supp=np.zeros((Nc,nstep))
		for N in range(0,Nc):
			supp[N,:]=np.linspace(AA[ind[N]],AA[maxmin[N+Nc*j][r]],nstep)
			freq[N,:]=np.interp(supp[N,:],AA[ind[N]:maxmin[Nc*j+N][r]+1],Freq[N][ind[N]:maxmin[Nc*j+N][r]+1])
			amp[N,:]=np.interp(supp[N,:],AA[ind[N]:maxmin[Nc*j+N][r]+1],Amp[Nc*j+N][ind[N]:maxmin[Nc*j+N][r]+1])
			ind[N]=maxmin[N+j*Nc][r]
		BetaFreq=np.zeros((supp.shape[1],P+1))
		BetaAmp=np.zeros((supp.shape[1],P+1))
		for p in range(0,P+1):
			for i in range(0,Nc):
				BetaFreq[:,p]=BetaFreq[:,p] + freq[i,:]*weights[i]*PSI[p,i] /normfact[p]
				BetaAmp[:,p]=BetaAmp[:,p] + amp[i,:]*weights[i]*PSI[p,i] /normfact[p]
		FREQ=np.dot(BetaFreq,psi)	
		AMP=np.dot(BetaAmp,psi)
		meanfreq=np.hstack((meanfreq,BetaFreq[:,0]))
		meanamp=np.hstack((meanamp,BetaAmp[:,0]))
		stdamp=np.hstack((stdamp,np.std(AMP,axis=1)))
		stdfreq=np.hstack((stdfreq,np.std(FREQ,axis=1)))
		#stdfreq=np.hstack((stdfreq,(np.dot(BetaFreq[:,1:]**2,normfact[1:,:])[:,0])**0.5 ))
		#stdamp=np.hstack((stdamp,(np.dot(BetaAmp[:,1:]**2,normfact[1:,:])[:,0])**0.5 ))
		perfrequp=np.hstack((perfrequp,np.percentile(FREQ,percen,axis=1)))
		perampup=np.hstack((perampup,np.percentile(AMP,percen,axis=1)))
		perfreqbot=np.hstack((perfreqbot,np.percentile(FREQ,100-percen,axis=1)))
		perampbot=np.hstack((perampbot,np.percentile(AMP,100-percen,axis=1)))
		for l in range(0,Ncourves):
			plt.figure(j)
			p1,=plt.plot(FREQ[:,l],AMP[:,l],'c')

	p2,=plt.plot(meanfreq,meanamp,'y',linewidth=2)
	p3,=plt.plot(meanfreq+stdfreq,meanamp+stdamp,colstd,linewidth=1.5)
	#if j==1: 
		#np.save(obFile,meanfreq)
		#np.save(obFile,meanamp)
	plt.plot(meanfreq-stdfreq,meanamp-stdamp,colstd,linewidth=1.5)
	p4,=plt.plot(perfrequp,perampup,colper,linewidth=1.5,linestyle='--')
	plt.plot(perfreqbot,perampbot,colper,linewidth=1.5,linestyle='--')
	plotlegend[j]=[p1,p2,p3,p4]


#obFile.close()

for j in Dofs:
	plt.figure(j)
	plt.ylim([1e-7,1e-2])
	plt.xlim((0,lastfreq))
	plt.title("DOF nb."+str(j+1))
	plt.xlabel("Excitation frequency [Hz]")
	plt.ylabel("Amplitude [m]")
	plt.yscale('log')
	plt.legend(plotlegend[j],["Stochastic response","Mean","Mean + - std. dev.","95% and 5% percentiles"],loc=4)
plt.show()


