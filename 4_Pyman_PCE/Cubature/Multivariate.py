import numpy as np, GQuad
from openturns import *

def get_multivar(DIM,deg,P,nodes,family):
	PSI=np.ones((P+1,nodes.shape[1]))
	normfact=np.ones((P+1,1))
	PSI_uni=[None]*DIM
	norm_uni=[None]*DIM
	for i in range(0,DIM):
		PSI_uni[i]=GQuad.get_PSI(nodes[i,:],deg,family[i])
		norm_uni[i]=GQuad.get_normfact(deg,family[i])
		
	for p in range(0,P+1):
		Ind=EnumerateFunction(DIM)(p)
		for i in range(0,DIM): 
			PSI[p,:]=np.multiply(PSI[p,:],PSI_uni[i][Ind[i],:])
			normfact[p]=np.multiply(normfact[p],norm_uni[i][Ind[i]])
	
	return PSI,normfact
		
	
