import numpy as np,GQuad, math
from sys import exit

def grid(DIM,lev,family): #lev=deg-1 #family=["hermite"]*12
	tol=np.spacing(1)
	Ntot = sparse_grid_size_total (DIM, lev, family)
	
	XX,WW=sparse_grid( DIM, lev, family,tol,Ntot)
	
	return XX,WW

def sparse_grid (dim_num, level_max, family,tol,point_total_num):
	sparse_total_order = np.zeros (( dim_num, point_total_num ))
	sparse_total_index = np.zeros (( dim_num, point_total_num ))
	sparse_total_point = np.ones(( dim_num, point_total_num ))*1e30
	sparse_total_weight=np.zeros((dim_num, point_total_num))
	Coeffs=np.zeros(point_total_num)
	
	point_total_num2 = 0
	level_min = max ( 0, level_max + 1 - dim_num )
	tol=tol**(0.5)
	
	for level in range(level_min,level_max+1):
		level_1d = np.zeros(0)
		more_grids = 0
		h = 0
		t = 0
		while 1 :
			level_1d, more_grids, h, t= comp_next ( level,dim_num,level_1d,more_grids, h, t )
			order=np.zeros(dim_num)
			for i in range(0,dim_num): order[i] = 2*level_1d[i] + 1
			coeff=smolyak_coeff(dim_num-1,level_max,level)
			point_index = np.zeros(0)
			more_points = 0
			while 1:
				point_index, more_points= vec_colex_next3 ( dim_num, order,point_index, more_points )
				if more_points==0: break
				point_total_num2 = point_total_num2 + 1
				Coeffs[point_total_num2-1]=coeff
				sparse_total_order[0:dim_num,point_total_num2-1] = order[0:dim_num]
				sparse_total_index[0:dim_num,point_total_num2-1] = point_index[0:dim_num]
			if more_grids==0: break
	#Now compute the coordinates and the weights of the TOTAL set of points
	
	for dim in range(0,dim_num):
		for level in range(0,level_max+1):
			order=2*level + 1
			points,ww=GQuad.collocation(order,family[dim])
			index = np.array(np.nonzero(sparse_total_order[dim,:] == order ))[0,:]
			indp=sparse_total_index[dim,index]-1
			for j in range(0,len(indp)): 
				sparse_total_point[dim,index[j]]=points[int(indp[j])]
				sparse_total_weight[dim,index[j]]=ww[int(indp[j])]
	
	sparse_total_point=np.around(sparse_total_point, decimals=12)
	sorting=np.lexsort((sparse_total_point[::-1]))
	sparse_total_point=sparse_total_point[:,sorting]
	ww=(np.prod(sparse_total_weight,axis=0))[sorting]
	Coeffs=Coeffs[sorting]
	ww=np.multiply(ww,Coeffs)
	
	und,redmap=undex ( dim_num, point_total_num, sparse_total_point, tol )   #unique points
	redmap=redmap.astype(int)
	und=und.astype(int)

	X=sparse_total_point[:,redmap]
	
	W=np.zeros(X.shape[1])
	for i in range(0,int(point_total_num)): W[und[i]]=W[und[i]]+ww[i]
	return X,W

def smolyak_coeff(dim_num,level_max,level):
	if ( level_max-level == 0 ): coeff = 1.0
	elif (level_max-level == 1 ): coeff = dim_num
	elif (level_max-level == dim_num-1 ): coeff = dim_num
	elif (level_max-level == dim_num): coeff = 1.0
	elif ( 1 < level_max-level and level_max-level < dim_num-1 ): 
		import scipy.special
		coeff=scipy.special.binom(dim_num,level_max-abs(level))
	else: coeff=0.0
	return coeff*(-1)**(level_max-abs(level))

def undex ( x_dim, x_num, x_val,tol ):
	indx = np.arange(0,x_num)
	i = 0
	j = 0
	undx=np.zeros(0)
	xdnu=np.zeros(0)
	
	undx= np.hstack((undx,[indx[i]]))
	xdnu= np.hstack((xdnu,[indx[i]]))
	for i in range(1, int(x_num)):
		if ( tol < max ( abs ( x_val[:,indx[i]] - x_val[:,undx[j]] ) ) ):
			j = j + 1
			undx=np.hstack((undx,[indx[i]]))
		xdnu= np.hstack((xdnu,[j]))
			
	return xdnu,undx

def sorted_unique(m, n, a, tol ):
	unique_num = 1
	j1 = 0
	for j2 in range (1,int(n)):
		#print max ( abs ( (a[:,j1] - a[:,j2] ) ) ) 
		if tol < max ( abs  (a[:,j1] - a[:,j2] ) ) :
			unique_num = unique_num + 1
			j1 = j2
	return unique_num

def sparse_grid_size_total (DIM, lev, family ):
	point_total_num = 0
	level_min = max ( 0, lev+ 1 - DIM )
	for level in range(level_min,lev+1):
		level_1d = np.zeros(0)
		more_grids = 0
		h = 0
		t = 0
		while 1:
			level_1d, more_grids, h, t= comp_next ( level,DIM,level_1d,more_grids, h, t )
			order=np.zeros(DIM)
			for i in range(0,DIM): order[i] = 2*level_1d[i] + 1
			point_total_num = point_total_num + np.prod (order[0:DIM] )
			if ( more_grids==0 ): break
	return point_total_num

def vec_colex_next3 ( dim_num, base, a, more ):
	
	if  more==0:
		a= np.ones(dim_num)
		more = 1
	else:
		for i in range(0, dim_num):
			a[i]=a[i] + 1
			if ( a[i] <= base[i] ): 
				return a, more
			a[i] = 1
		more = 0

	return a,more

def comp_next( n, k, a, more, h, t ):
	if more==0:
		t = n
		h = 0
		a=np.zeros(k)
		a[0]=n
	else:
		if ( 1 < t ): h = 0
		h = h + 1
		t = a[h-1]
		a[h-1] = 0
		a[0] = t - 1
		a[h] = a[h] + 1
	
	more = ( a[k-1] != n )
	return a, more, h, t
