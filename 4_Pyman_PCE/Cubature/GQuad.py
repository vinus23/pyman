from openturns import *
import numpy
from math import floor

def collocation(Nc,family):
	if family=="hermite":
		hermite = HermiteFactory()
		weights = NumericalPoint()
		nodes = hermite.getNodesAndWeights(Nc, weights)
	if family=="legendre":
		legendre=LegendreFactory()
		weights = NumericalPoint()
		nodes =legendre.getNodesAndWeights(Nc, weights)
		weights=weights*2
	return nodes,weights

def get_PSI(nodes,P,family):
	PSI=numpy.zeros(( P+1,len(nodes) ))
	if family=="hermite":
		from numpy.polynomial.hermite_e import hermeval
		for i in range(0,len(nodes)):
			node=nodes[i]
			for j in range(0,P+1): PSI[j,i]=hermeval(node,numpy.hstack((numpy.zeros(j),1)))
	if family=="legendre":
		from numpy.polynomial.legendre import legval
		for i in range(0,len(nodes)):
			node=nodes[i]
			for j in range(0,P+1): PSI[j,i]=legval(node,numpy.hstack((numpy.zeros(j),1)))
	return PSI

def get_normfact(P,family):
	import math
	normfact=numpy.zeros((P+1,1))
	if family=="hermite":
		for i in range(0,P+1):normfact[i]=math.factorial(i)
	if family=="legendre":
		for i in range(0,P+1):normfact[i]=1.0/(2*i+1.0)*2
	return normfact
