###ANM
def start(M,K,C,Gamma,ff,Nc,finalfreq):
	import os, sys
	sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Pyman_func" )
	import random,numpy,SysHB,Preliminar,Man
	dof=M.shape[0]
	invM=numpy.zeros((dof,dof,Nc))
	for i in range(0,Nc):  
		invM[:,:,i]=numpy.linalg.inv(M[:,:,i])
		ff[:,:,i]=numpy.dot(invM[:,:,i],ff[:,:,i])
	###
	H=3       #nb of harmonics
	Hdisp=[1,1]   #harmonics we want to display
	ukvar=[1,2]  #uknown we want to dispay, (same size for Hdisp and ukvar!!!)
	###
	Neq=3*dof #n of equations
	ninc=Neq*(2*H+1)+1 #variables (all the harmonics + the frequency)
	neqsys=ninc-1
	### 
	randvect=numpy.random.rand(1,ninc) # it will be used for tangent calculation...see in SYSHB.py
	###ANM parameters
	chemin = numpy.ones((ninc,1)) # default path vector (weighting function)
	ordre = 15;            					#default ANM series order
	nbptstroncon = 20    					#default sampling of portions for display
	seuil = 1e-7         					#default ANM threshold
	itemax = 15            					#default max. iteration for N-R correction
	seuilcorr = 1.0e-8      				#default threshold for N-R correction
	stepcorr=0    							#put 1 to correct at each step
	ntronc=5000					#Nb of steps MAX
	###
	###Perturbation NOT USED HERE
	cfp = 0.0            #switch to 1 (or -1) if you want to perturb
	Pertampl=1.0e-6
	Fp = numpy.random.rand(neqsys,1) # perturbation vector
	Fp = Fp/numpy.linalg.norm(Fp) # normalization
	Pert=cfp*Pertampl*Fp
	###Stability NOT USED HERE
	SS=0    			#put to 1 if you want the stability analysis
	varstab = numpy.hstack(( numpy.arange(1,dof+1) , numpy.array([Neq*(2*H+1)+1, Neq*(2*H+1)+1, H]) )) #Variables used for stability analysis: Dispacements, frequency 
	tolstab = 1e-4    #tol used in stability analysis
	###
	Ustart  = numpy.zeros((Neq*(2*H+1)+1,1,Nc))
	Ustart[-1,0,:]= 0.01*2*3.14 #Starting Frequency!!! in rad/s
	###STARTING###
	U=Ustart
	Ut  = numpy.zeros((Neq*(2*H+1)+1,1,Nc))
	for i in range(0,Nc): U[:,:,i]=Preliminar.correction(U[:,:,i],H,Neq,ninc,neqsys,invM[:,:,i],K[:,:,i],C[:,:,i],Gamma[:,:,i],ff[:,:,i],Pert,randvect,chemin,seuilcorr,itemax)
	for i in range(0,Nc): Ut[:,:,i]=SysHB.get_Ut(U[:,:,i],SysHB.get_dRdU(U[:,:,i],H,Neq,ninc,neqsys,invM[:,:,i],K[:,:,i],C[:,:,i],Gamma[:,:,i],ff[:,:,i]),ninc,randvect,chemin)
	obFile=[]
	for i in range(0,Nc): obFile.append(    open(os.path.dirname(os.path.realpath(__file__))+"/Data/"+"values"+str(i+1),'a')   )
	fileA=[]
	for i in range(0,Nc): fileA.append(    open(os.path.dirname(os.path.realpath(__file__))+"/Data/"+"A"+str(i+1),'a')   )
	
	Man.Branche(U,-Ut,ntronc ,H,Neq,ninc,neqsys,invM,K,C,Gamma,ff,Pert,randvect,chemin,seuil,seuilcorr,itemax,ordre,nbptstroncon,varstab,tolstab,stepcorr,Hdisp,finalfreq,SS,ukvar,obFile,fileA,Nc)
	
