import numpy as np, PYMAN, os, sys, matplotlib.pyplot as plt
from sys import exit
from math import pi
import scipy.signal
from openturns import *
from numpy.polynomial.hermite_e import hermeval
from numpy.polynomial.legendre import legval
folder="/Data"
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+folder)
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Cubature")
import GQuad
#RandomGenerator.SetSeed(0)
family="legendre"

Ncourves=1000 #nb of courves we want to get
if family=="hermite":  dist1=Normal(0,1)   #distribution of the random variable
if family=="legendre":  dist1=Uniform(-1,1)   
RandomExp1 = MonteCarloExperiment(dist1,Ncourves) #generate a samplelike for the MCS...it's useful to compare PCE to MCS
xi=RandomExp1 .generate( )   #random variable

#Collocation points, nb. of files in the folder (values1...valuesNc)
Nc=9
#PCE order, it must be smaller than Nc (for Gaussian quadrature)
P=6
M=4
L=P-M
FILTER=0 #put to 1 if you want the filter
randvect=np.random.rand(1,L+1)
#DOF
dof=2
#last frequency we want to arrive at
lastfreq=55   #in Hz

#Calculate nodes and weights for Gaussian quadrature
nodes,weights=GQuad.collocation(Nc,family)
W=np.matrix(weights).T
normfact=GQuad.get_normfact(P,family)
PSI=GQuad.get_PSI(nodes,P,family)  #evaluate the Polynomial basis in the collocation points

#xi=np.array(nodes)
#Ncourves=Nc

################## 1 ### Reading the files in which the output is stored
print "Reading the files"
Ntr=np.zeros(Nc)
for i in range(0,Nc): Ntr[i]=(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1)).shape[0]/(dof*2+1))
nbptstroncon =np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(1)).shape[1]
Ntr=Ntr.astype(int)

Freq=[None]*Nc
Hcos=[None]*dof*Nc
Hsin=[None]*dof*Nc
aa=[None]*Nc
finalA=np.zeros(Nc)

for i in range(0,Nc):
	Freq[i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1))[dof*2::dof*2+1,:]/2.0/pi, (Ntr[i]*nbptstroncon,1))[:,0]
	aa[i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/A"+str(i+1)), (Ntr[i]*nbptstroncon,1))[:,0]
	finalA[i]=aa[i][-1]
	for j in range(0,dof): 
		Hcos[Nc*j+i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1))[j::dof*2+1,:], (Ntr[i]*nbptstroncon,1))[:,0]
		Hsin[Nc*j+i]=np.reshape(np.loadtxt(os.path.dirname(os.path.realpath(__file__))+folder+"/values"+str(i+1))[j+dof::dof*2+1,:], (Ntr[i]*nbptstroncon,1))[:,0]

finalA=min(finalA)

color=['g','r']
AA=np.arange(0,finalA,finalA/(max(Ntr)*nbptstroncon))
for i in range(0,Nc):
	Freq[i]=np.interp(AA,aa[i],Freq[i])
	for j in range(0,dof): 
		Hcos[Nc*j+i]=np.interp(AA,aa[i],Hcos[Nc*j+i])
		Hsin[Nc*j+i]=np.interp(AA,aa[i],Hsin[Nc*j+i])
		plt.figure(1)
		plt.plot(AA,Hcos[Nc*j+i],color[j])
		#plt.yscale('log')
		plt.figure(2)
		plt.plot(AA,Hsin[Nc*j+i],color[j])
		#plt.yscale('log')
		plt.figure(3)
		#if j==0 : plt.plot(Freq[i],(Hcos[Nc*j+i]**2+Hsin[Nc*j+i]**2)**0.5,color[j])
		plt.yscale('log')		
	plt.figure(4)
	plt.plot(AA,Freq[i],color[j])
Hc=Hcos
Hs=Hsin
#print np.array(np.nonzero((np.diff((np.diff(Hcos[0])>0)+0)==-1)+0))[0]+1
#print np.array(np.nonzero((np.diff((np.diff(Hcos[0])>0)+0)==1)+0))[0]+1

################## 2 ### Calculate the stochastic modes
print "Calculating the stochastic modes and the response"
PHcos=np.zeros((len(AA),(M+1),dof ))
PHsin=np.zeros((len(AA),(M+1),dof ))
QHcos=np.zeros((len(AA),(L+1),dof ))
QHsin=np.zeros((len(AA),(L+1),dof ))
BetaFreq=np.zeros((len(AA),P+1))

for p in range(0,P+1):              # the frequency (stochastic) is calculated with PCE
	for i in range(0,Nc): BetaFreq[:,p]=BetaFreq[:,p] + Freq[i]*weights[i]*PSI[p,i] /normfact[p]
		
psi=np.zeros((P+1,Ncourves))
for l in range(0,Ncourves):
	for p in range(0,P+1): 
		if family=="hermite": psi[p,l]=hermeval(xi[l],np.hstack((np.zeros(p),1)))
		if family=="legendre": psi[p,l]=legval(xi[l],np.hstack((np.zeros(p),1)))

Freq=np.dot(BetaFreq,psi) 

B=np.multiply(PSI[P-L+1:,:],np.tile(W.T,(L,1)))
C=np.matrix(np.eye(Nc))
D=(PSI[:L +1,:]).T
b=np.zeros((L+1,1))
b[-1]=1
vect=np.zeros((1,L+1))
vect[0]=1

for i in range(0,len(AA)):
	for j in range(0,dof):
		Ucos=np.zeros((Nc,1))
		Usin=np.zeros((Nc,1))
		for c in range(0,Nc):
			Ucos[c]=Hcos[Nc*j+c][i]
			Usin[c]=Hsin[Nc*j+c][i]
		Acos=np.vstack(( np.dot(B,np.dot(np.multiply(C,Ucos),D)) , vect))
		Asin=np.vstack(( np.dot(B,np.dot(np.multiply(C,Usin),D)) , vect))
		qcos=np.linalg.solve(Acos,b)
		QHcos[i,:,j]=qcos.T[0,:]
		qsin=np.linalg.solve(Asin,b)
		QHsin[i,:,j]=qsin.T[0,:]
		#Q calculated, now let's calculate P
		PHcos[i,:,j]=(np.dot(PSI[:M+1,:],np.multiply(np.multiply(np.dot(PSI[:L+1,:].T,np.reshape(QHcos[i,:,j],(L+1,1))),W),Ucos))/normfact[:M+1]).T
		PHsin[i,:,j]=(np.dot(PSI[:M+1,:],np.multiply(np.multiply(np.dot(PSI[:L+1,:].T,np.reshape(QHsin[i,:,j],(L+1,1))),W),Usin))/normfact[:M+1]).T

Hcos=np.zeros((len(AA),Ncourves,dof))
Hsin=np.zeros((len(AA),Ncourves,dof))

for j in range(0,dof):
	Hcos[:,:,j]=np.dot(PHcos[:,:,j],psi[:M+1,:])/np.dot(QHcos[:,:,j],psi[:L+1,:])
	Hsin[:,:,j]=np.dot(PHsin[:,:,j],psi[:M+1,:])/np.dot(QHsin[:,:,j],psi[:L+1,:])

###3 ### Median Filter
ker=2*int(Ncourves/(2*P))+1
if FILTER==1:
	print "Applying median filter"
	for i in range(0,len(AA)):
		for j in range(0,dof):
			absQcos=abs(np.dot(QHcos[i,:,j],psi[:L+1,:]))
			absQsin=abs(np.dot(QHsin[i,:,j],psi[:L+1,:]))
			Hcos[i,:,j]=scipy.signal.medfilt(np.multiply(Hcos[i,:,j],absQcos),ker)   /   scipy.signal.medfilt(absQcos,ker)
			Hsin[i,:,j]=scipy.signal.medfilt(np.multiply(Hsin[i,:,j],absQsin),ker)   /   scipy.signal.medfilt(absQsin,ker)

###4 ### Final plots
plt.figure(4)  ###Frequency
for l in range(0,Ncourves): plt.plot(AA,Freq[:,l],'c')

color=['c','y']
for j in range(0,dof):
	for l in range(0,Ncourves):
		plt.figure(1) 
		plt.plot(AA,Hcos[:,l,j],color[j])
		plt.figure(2) 
		plt.plot(AA,Hsin[:,l,j],color[j])
		plt.figure(3) 
		if j==0: 
			plt.plot(Freq[:,l],(Hsin[:,l,j]**2+Hcos[:,l,j]**2)**0.5,color[j])
			plt.ylim((1e-7,1e-2))
			plt.xlabel("Excitation frequency [Hz]")
			plt.ylabel("Amplitude [m]")
plt.figure(5)   ###See what is happening in a point (arc lenght value)
point=92
j=1
for i in range(0,Nc): plt.plot(nodes[i],Hc[Nc*j+i][point/max(AA)*len(AA)],'o')
plt.plot(xi,Hcos[point/max(AA)*len(AA),:,j])

plt.show()

