import numpy, PYMAN,scipy.special as sp
from sys import exit
import matplotlib.pyplot as plt, time
from math import *
from openturns import *
from mpl_toolkits.mplot3d import Axes3D
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Data" )
sys.path.append( (os.path.dirname(os.path.realpath(__file__)))+"/Cubature" )
import GQuad, Smolyak
START=time.clock()

#Random space dimension and orthogonal polynomials for each dimension
DIM=2
deg= 3 #polynomial degree
family=["legendre","hermite"]
#Degrees of freedom
dof=2
#Deterministic parameters
m=1.0
knl=5.0e10
c=1.0
k=15000.0

last=60 #last frequency

#Uncertain parameters
mean=numpy.array([k,knl,m])
disp=numpy.array([0.025,0.1,0.075])

#PCE order
P=factorial(deg+DIM)/factorial(deg)/factorial(DIM)-1

#Collocation points
if DIM==1:
	Nc=2*deg+1
	nodes,weights=GQuad.collocation(Nc,family[0])
elif DIM>1:
	lev=deg-1
	nodes,weights=Smolyak.grid(DIM,lev,family)
	Nc=nodes.shape[1]

K=numpy.zeros((dof,dof,Nc))
C=numpy.zeros((dof,dof,Nc))
M=numpy.zeros((dof,dof,Nc))
Gamma=numpy.zeros((dof,dof,Nc))
ff=numpy.zeros((dof,1,Nc))

print nodes.shape
print sum(weights)
plt.figure(1)
plt.plot(nodes[0,:],nodes[1,:],'ro',markersize=9)

plt.show()
exit()
#plt.xlabel("GAUSSIAN")
#plt.ylabel("UNIFORM")
#nodes1,weights=GQuad.collocation(7,family[0])
#nodes2,weights=GQuad.collocation(7,family[1])
#nodes1=numpy.array(nodes1)
#nodes2=numpy.array(nodes2)
#nodes1=numpy.tile(nodes1,len(nodes1))
#nodes2=numpy.sort(numpy.tile(nodes2,len(nodes2)))
#plt.figure(2)
#plt.plot(nodes1,nodes2,'go',markersize=9)
#plt.xlabel("GAUSSIAN")
#plt.ylabel("UNIFORM")
#plt.figure(3)
#plt.plot(nodes1,nodes2,'go',markersize=9)
#plt.plot(nodes[0,:],nodes[1,:],'ro',markersize=9)



#solve deterministic problem in collocation points
for n in range(0,Nc):
	#uncertain variable evaluated in coll. points
	if DIM==1: 
		randvars=numpy.array([mean[0]*(1+disp[0]*nodes[n])])
	else:
		 randvars=mean*(1+disp*nodes[:,n])
	
	k=randvars[0]
	knl=randvars[1]
	#m=randvars[2]
	#mass, damping, stiffness, nonlinear matrices and force vector
	K[:,:,n]=numpy.array([[2*k,-k],[-k,k]])
	C[:,:,n]=numpy.array([[2*c,-c],[-c,c]])
	M[:,:,n]=numpy.array([[m,0],[0,m]])
	Gamma[:,:,n]=numpy.eye(dof)*0
	Gamma[0,0,n]=knl
	ff[:,:,n]=numpy.zeros((dof,1))   
	ff[0,0,n]=1		#Put a force on one or more dof

PYMAN.start(M,K,C,Gamma,ff,Nc,last)

FINISH=time.clock()
print "Time elapsed:", FINISH-START, "seconds"


